-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema tcab_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tcab_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tcab_db` DEFAULT CHARACTER SET utf8 ;
USE `tcab_db` ;

-- -----------------------------------------------------
-- Table `tcab_db`.`actionpoint`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`actionpoint` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `mobile` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `addressline1` VARCHAR(255) NOT NULL,
  `addressline2` VARCHAR(45) NULL DEFAULT NULL,
  `addressline3` VARCHAR(45) NULL DEFAULT NULL,
  `state` VARCHAR(255) NULL DEFAULT NULL,
  `country` VARCHAR(255) NOT NULL,
  `active` TINYINT(1) NOT NULL,
  `requiredChangePassword` TINYINT(1) NULL DEFAULT NULL,
  `dob` DATE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 47
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`supervisor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`supervisor` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `officelocation` VARCHAR(255) NOT NULL,
  `user` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_supervisor__user` (`user` ASC),
  CONSTRAINT `fk_supervisor__user`
    FOREIGN KEY (`user`)
    REFERENCES `tcab_db`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`unit` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `unitcode` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NULL,
  `startdate` DATE NOT NULL,
  `enddate` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`project` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` MEDIUMBLOB NOT NULL,
  `maximunmembers` INT(11) NOT NULL,
  `unit` INT(11) NULL DEFAULT NULL,
  `descriptionFileType` VARCHAR(255) NULL DEFAULT NULL,
  `descriptionFileName` VARCHAR(255) NULL DEFAULT NULL,
  `startdate` DATE NOT NULL,
  `enddate` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_project__unit` (`unit` ASC),
  CONSTRAINT `fk_project__unit`
    FOREIGN KEY (`unit`)
    REFERENCES `tcab_db`.`unit` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`team` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `project` INT(11) NULL DEFAULT NULL,
  `supervisor` INT(11) NULL DEFAULT NULL,
  `teamcount` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_team__project` (`project` ASC),
  INDEX `idx_team__supervisor` (`supervisor` ASC),
  CONSTRAINT `fk_team__project`
    FOREIGN KEY (`project`)
    REFERENCES `tcab_db`.`project` (`id`),
  CONSTRAINT `fk_team__supervisor`
    FOREIGN KEY (`supervisor`)
    REFERENCES `tcab_db`.`supervisor` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`meeting`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`meeting` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(255) NOT NULL,
  `location` VARCHAR(255) NOT NULL,
  `time` DATETIME NOT NULL,
  `munites` BLOB NOT NULL,
  `supervisorcomments` VARCHAR(255) NOT NULL,
  `team` INT(11) NOT NULL,
  `supervisor` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_meeting__supervisor` (`supervisor` ASC),
  INDEX `idx_meeting__team` (`team` ASC),
  CONSTRAINT `fk_meeting__supervisor`
    FOREIGN KEY (`supervisor`)
    REFERENCES `tcab_db`.`supervisor` (`id`),
  CONSTRAINT `fk_meeting__team`
    FOREIGN KEY (`team`)
    REFERENCES `tcab_db`.`team` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`actionpoint_meeting`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`actionpoint_meeting` (
  `actionpoint` INT(11) NOT NULL,
  `meeting` INT(11) NOT NULL,
  PRIMARY KEY (`actionpoint`, `meeting`),
  INDEX `idx_actionpoint_meeting` (`meeting` ASC),
  CONSTRAINT `fk_actionpoint_meeting__actionpoint`
    FOREIGN KEY (`actionpoint`)
    REFERENCES `tcab_db`.`actionpoint` (`id`),
  CONSTRAINT `fk_actionpoint_meeting__meeting`
    FOREIGN KEY (`meeting`)
    REFERENCES `tcab_db`.`meeting` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`agendaitem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`agendaitem` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`agendaitem_meeting`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`agendaitem_meeting` (
  `agendaitem` INT(11) NOT NULL,
  `meeting` INT(11) NOT NULL,
  PRIMARY KEY (`agendaitem`, `meeting`),
  INDEX `idx_agendaitem_meeting` (`meeting` ASC),
  CONSTRAINT `fk_agendaitem_meeting__agendaitem`
    FOREIGN KEY (`agendaitem`)
    REFERENCES `tcab_db`.`agendaitem` (`id`),
  CONSTRAINT `fk_agendaitem_meeting__meeting`
    FOREIGN KEY (`meeting`)
    REFERENCES `tcab_db`.`meeting` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`convener`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`convener` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_convener__user` (`user` ASC),
  CONSTRAINT `fk_convener__user`
    FOREIGN KEY (`user`)
    REFERENCES `tcab_db`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`employee_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`employee_unit` (
  `user` INT(11) NOT NULL,
  `unit` INT(11) NOT NULL,
  INDEX `fk_user_unit_idx` (`user` ASC),
  INDEX `fk_unit_idx` (`unit` ASC),
  CONSTRAINT `fk_unit`
    FOREIGN KEY (`unit`)
    REFERENCES `tcab_db`.`unit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user`
    FOREIGN KEY (`user`)
    REFERENCES `tcab_db`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`hibernate_sequence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`hibernate_sequence` (
  `next_val` BIGINT(20) NULL DEFAULT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`student` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_student__user` (`user` ASC),
  CONSTRAINT `fk_student__user`
    FOREIGN KEY (`user`)
    REFERENCES `tcab_db`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`meeting_student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`meeting_student` (
  `meeting` INT(11) NOT NULL,
  `student` INT(11) NOT NULL,
  PRIMARY KEY (`meeting`, `student`),
  INDEX `idx_meeting_student` (`student` ASC),
  CONSTRAINT `fk_meeting_student__meeting`
    FOREIGN KEY (`meeting`)
    REFERENCES `tcab_db`.`meeting` (`id`),
  CONSTRAINT `fk_meeting_student__student`
    FOREIGN KEY (`student`)
    REFERENCES `tcab_db`.`student` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`peerassessment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`peerassessment` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `duedate` DATE NOT NULL,
  `submitteddate` DATE NOT NULL,
  `team` INT(11) NOT NULL,
  `student` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_peerassessment__student` (`student` ASC),
  INDEX `idx_peerassessment__team` (`team` ASC),
  CONSTRAINT `fk_peerassessment__student`
    FOREIGN KEY (`student`)
    REFERENCES `tcab_db`.`student` (`id`),
  CONSTRAINT `fk_peerassessment__team`
    FOREIGN KEY (`team`)
    REFERENCES `tcab_db`.`team` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `associatingcost` DOUBLE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`project_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`project_role` (
  `project` INT(11) NOT NULL,
  `role` INT(11) NOT NULL,
  PRIMARY KEY (`project`, `role`),
  INDEX `idx_project_role` (`role` ASC),
  CONSTRAINT `fk_project_role__project`
    FOREIGN KEY (`project`)
    REFERENCES `tcab_db`.`project` (`id`),
  CONSTRAINT `fk_project_role__role`
    FOREIGN KEY (`role`)
    REFERENCES `tcab_db`.`role` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`task` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `deacription` VARCHAR(255) NOT NULL,
  `role` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_task__role` (`role` ASC),
  CONSTRAINT `fk_task__role`
    FOREIGN KEY (`role`)
    REFERENCES `tcab_db`.`role` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`project_task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`project_task` (
  `project` INT(11) NOT NULL,
  `task` INT(11) NOT NULL,
  PRIMARY KEY (`project`, `task`),
  INDEX `idx_project_task` (`task` ASC),
  CONSTRAINT `fk_project_task__project`
    FOREIGN KEY (`project`)
    REFERENCES `tcab_db`.`project` (`id`),
  CONSTRAINT `fk_project_task__task`
    FOREIGN KEY (`task`)
    REFERENCES `tcab_db`.`task` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`student_task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`student_task` (
  `student` INT(11) NOT NULL,
  `task` INT(11) NOT NULL,
  `timetaken` INT(11) NOT NULL,
  `date` DATE NOT NULL,
  `week` INT(11) NOT NULL,
  `team` INT(11) NOT NULL,
  PRIMARY KEY (`student`, `task`, `team`),
  INDEX `idx_student_task` (`task` ASC),
  INDEX `fk_student_task_team_idx` (`team` ASC),
  CONSTRAINT `fk_student_task__student`
    FOREIGN KEY (`student`)
    REFERENCES `tcab_db`.`student` (`id`),
  CONSTRAINT `fk_student_task__task`
    FOREIGN KEY (`task`)
    REFERENCES `tcab_db`.`task` (`id`),
  CONSTRAINT `fk_student_task_team`
    FOREIGN KEY (`team`)
    REFERENCES `tcab_db`.`team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`student_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`student_unit` (
  `student` INT(11) NOT NULL,
  `unit` INT(11) NOT NULL,
  PRIMARY KEY (`student`, `unit`),
  INDEX `idx_student_unit` (`unit` ASC),
  CONSTRAINT `fk_student_unit__student`
    FOREIGN KEY (`student`)
    REFERENCES `tcab_db`.`student` (`id`),
  CONSTRAINT `fk_student_unit__unit`
    FOREIGN KEY (`unit`)
    REFERENCES `tcab_db`.`unit` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`team_student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`team_student` (
  `student` INT(11) NOT NULL,
  `team` INT(11) NOT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  INDEX `fk_student_team_idx` (`student` ASC),
  INDEX `fk_team_student_idx` (`team` ASC),
  CONSTRAINT `fk_student_team`
    FOREIGN KEY (`student`)
    REFERENCES `tcab_db`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_team_student`
    FOREIGN KEY (`team`)
    REFERENCES `tcab_db`.`team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 29
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`userrole`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`userrole` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tcab_db`.`user_userrole`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`user_userrole` (
  `user` INT(11) NOT NULL,
  `userrole` INT(11) NOT NULL,
  PRIMARY KEY (`user`, `userrole`),
  INDEX `idx_user_userrole` (`userrole` ASC),
  CONSTRAINT `fk_user_userrole__user`
    FOREIGN KEY (`user`)
    REFERENCES `tcab_db`.`user` (`id`),
  CONSTRAINT `fk_user_userrole__userrole`
    FOREIGN KEY (`userrole`)
    REFERENCES `tcab_db`.`userrole` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



CREATE TABLE IF NOT EXISTS `tcab_db`.`upload_error` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `data` MEDIUMBLOB NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `tcab_db` ;

-- -----------------------------------------------------
-- Placeholder table for view `tcab_db`.`projectteamallocation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tcab_db`.`projectteamallocation` (`teamId` INT, `name` INT, `project` INT, `supervisorId` INT, `supervisorName` INT, `studentId` INT, `studentName` INT, `teamcount` INT);

-- -----------------------------------------------------
-- procedure searchProject
-- -----------------------------------------------------

DELIMITER $$
USE `tcab_db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `searchProject`(IN projectName varchar(45),
    IN unitId INT)
BEGIN
	SET @queryTest = 'SELECT DISTINCT p.id, p.name, p.maximunmembers, p.unit, u.unitcode, u.name AS unitName FROM project AS p INNER JOIN unit AS u ON p.unit = u.id WHERE (CURDATE() BETWEEN p.startdate AND p.enddate) AND 1 = 1 ';
        IF (projectName != '') THEN
			SET @queryTest = CONCAT(@queryTest, 'AND p.name ', 'LIKE \'%', projectName, '%\' ');
		END IF;
        IF (unitId > 0) THEN
			SET @queryTest = CONCAT(@queryTest, 'AND p.unit = ', unitId, ' ');
		END IF;
        SET @queryTest = CONCAT(@queryTest, 'ORDER BY p.unit');
        PREPARE stmt FROM @queryTest;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure searchUser
-- -----------------------------------------------------

DELIMITER $$
USE `tcab_db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `searchUser`(IN name varchar(45),
    IN email NVARCHAR(45), IN userType INT)
BEGIN
	SET @queryTest = 'SELECT DISTINCT u.* FROM user AS u INNER JOIN user_userrole AS ur ON u.id = ur.user 
					  WHERE 1 = 1 ';
        IF (name != '') THEN
			SET @queryTest = CONCAT(@queryTest, 'AND u.firstname ', 'LIKE \'%', name, '%\' OR u.lastname ', 'LIKE \'%', name, '%\' ');
		END IF;
		IF (email != '') THEN
			SET @queryTest = CONCAT(@queryTest, 'AND u.email ', 'LIKE \'%', email, '%\' ');
        END IF;
        IF (userType = 1) THEN
			SET @queryTest = CONCAT(@queryTest, 'AND (ur.userrole = 1 OR ur.userrole = 2 OR ur.userrole = 3) ');
		END IF;
        IF (userType = 4) THEN
			SET @queryTest = CONCAT(@queryTest, 'AND ur.userrole = 4 ');
		END IF;
        SET @queryTest = CONCAT(@queryTest, 'ORDER BY u.id');
        PREPARE stmt FROM @queryTest;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `tcab_db`.`projectteamallocation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tcab_db`.`projectteamallocation`;
USE `tcab_db`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tcab_db`.`projectteamallocation` AS select `t`.`id` AS `teamId`,`t`.`name` AS `name`,`t`.`project` AS `project`,`t`.`supervisor` AS `supervisorId`,`supervisor`.`supervisorName` AS `supervisorName`,`student`.`studentId` AS `studentId`,`student`.`studentName` AS `studentName`,`t`.`teamcount` AS `teamcount` from ((`tcab_db`.`team` `t` join (select `ts`.`team` AS `team`,`s`.`id` AS `studentId`,concat(`u`.`firstname`,' ',`u`.`lastname`) AS `studentName` from ((`tcab_db`.`team_student` `ts` join `tcab_db`.`student` `s` on((`ts`.`student` = `s`.`id`))) join `tcab_db`.`user` `u` on((`s`.`user` = `u`.`id`)))) `student` on((`t`.`id` = `student`.`team`))) join (select `su`.`id` AS `supervisorId`,concat(`u`.`firstname`,' ',`u`.`lastname`) AS `supervisorName` from (`tcab_db`.`supervisor` `su` join `tcab_db`.`user` `u` on((`su`.`user` = `u`.`id`)))) `supervisor` on((`t`.`supervisor` = `supervisor`.`supervisorId`)));

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
