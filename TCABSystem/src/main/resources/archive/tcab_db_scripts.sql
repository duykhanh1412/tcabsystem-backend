drop database if exists tcab_db;

-- create databases
create database tcab_db;

-- create table in tcab db
use tcab_db;

CREATE TABLE `actionpoint` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL
);

CREATE TABLE `agendaitem` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL
);

CREATE TABLE `role` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `associatingcost` DOUBLE NOT NULL
);

CREATE TABLE `task` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `deacription` VARCHAR(255) NOT NULL,
  `role` INTEGER NOT NULL
);

CREATE INDEX `idx_task__role` ON `task` (`role`);

ALTER TABLE `task` ADD CONSTRAINT `fk_task__role` FOREIGN KEY (`role`) REFERENCES `role` (`id`);

CREATE TABLE `unit` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `unitcode` VARCHAR(255) NOT NULL UNIQUE,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NULL,
  `startdate` DATE NOT NULL,
  `enddate` DATE NOT NULL
);

CREATE TABLE `project` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` MEDIUMBLOB NOT NULL,
  `maximunmembers` INTEGER NOT NULL,
  `unit` INTEGER,
  `descriptionFileType` VARCHAR(255),
  `descriptionFileName` VARCHAR(255),
  `startdate` DATE NOT NULL,
  `enddate` DATE NOT NULL
);

CREATE INDEX `idx_project__unit` ON `project` (`unit`);

ALTER TABLE `project` ADD CONSTRAINT `fk_project__unit` FOREIGN KEY (`unit`) REFERENCES `unit` (`id`);

CREATE TABLE `project_role` (
  `project` INTEGER NOT NULL,
  `role` INTEGER NOT NULL,
  PRIMARY KEY (`project`, `role`)
);

CREATE INDEX `idx_project_role` ON `project_role` (`role`);

ALTER TABLE `project_role` ADD CONSTRAINT `fk_project_role__project` FOREIGN KEY (`project`) REFERENCES `project` (`id`);

ALTER TABLE `project_role` ADD CONSTRAINT `fk_project_role__role` FOREIGN KEY (`role`) REFERENCES `role` (`id`);

CREATE TABLE `project_task` (
  `project` INTEGER NOT NULL,
  `task` INTEGER NOT NULL,
  PRIMARY KEY (`project`, `task`)
);

CREATE INDEX `idx_project_task` ON `project_task` (`task`);

ALTER TABLE `project_task` ADD CONSTRAINT `fk_project_task__project` FOREIGN KEY (`project`) REFERENCES `project` (`id`);

ALTER TABLE `project_task` ADD CONSTRAINT `fk_project_task__task` FOREIGN KEY (`task`) REFERENCES `task` (`id`);

CREATE TABLE `user` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL UNIQUE,
  `password` VARCHAR(255) NOT NULL,
  `dob` DATE NOT NULL,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `mobile` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL UNIQUE,
  `addressline1` VARCHAR(255) NOT NULL,
  `addressline2` VARCHAR(255) NULL,
  `addressline3` VARCHAR(255) NULL,
  `state` VARCHAR(255) NULL,
  `country` VARCHAR(255) NOT NULL,
  `active` BOOLEAN NOT NULL,
  `requiredChangePassword` BOOLEAN NOT NULL
);

CREATE TABLE `convener` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `user` INTEGER NOT NULL
);

CREATE INDEX `idx_convener__user` ON `convener` (`user`);

ALTER TABLE `convener` ADD CONSTRAINT `fk_convener__user` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

CREATE TABLE `supervisor` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `officelocation` VARCHAR(255) NOT NULL,
  `user` INTEGER NOT NULL
);

CREATE INDEX `idx_supervisor__user` ON `supervisor` (`user`);

ALTER TABLE `supervisor` ADD CONSTRAINT `fk_supervisor__user` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

CREATE TABLE `tcab_db`.`employee_unit` (
  `user` INT NOT NULL,
  `unit` INT NOT NULL,
  INDEX `fk_user_unit_idx` (`user` ASC),
  INDEX `fk_unit_idx` (`unit` ASC),
  CONSTRAINT `fk_user`
    FOREIGN KEY (`user`)
    REFERENCES `tcab_db`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_unit`
    FOREIGN KEY (`unit`)
    REFERENCES `tcab_db`.`unit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `team` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `project` INTEGER,
  `supervisor` INTEGER,
  `teamcount` INTEGER
);

CREATE INDEX `idx_team__project` ON `team` (`project`);

CREATE INDEX `idx_team__supervisor` ON `team` (`supervisor`);

ALTER TABLE `team` ADD CONSTRAINT `fk_team__project` FOREIGN KEY (`project`) REFERENCES `project` (`id`);

ALTER TABLE `team` ADD CONSTRAINT `fk_team__supervisor` FOREIGN KEY (`supervisor`) REFERENCES `supervisor` (`id`);

CREATE TABLE `tcab_db`.`team_student` (
  `student` INT NOT NULL,
  `team` INT NOT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  INDEX `fk_student_team_idx` (`student` ASC),
  INDEX `fk_team_student_idx` (`team` ASC),
  CONSTRAINT `fk_student_team`
    FOREIGN KEY (`student`)
    REFERENCES `tcab_db`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_team_student`
    FOREIGN KEY (`team`)
    REFERENCES `tcab_db`.`team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE `meeting` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `type` VARCHAR(255) NOT NULL,
  `location` VARCHAR(255) NOT NULL,
  `time` DATETIME NOT NULL,
  `munites` BLOB NOT NULL,
  `supervisorcomments` VARCHAR(255) NOT NULL,
  `team` INTEGER NOT NULL,
  `supervisor` INTEGER NOT NULL
);

CREATE INDEX `idx_meeting__supervisor` ON `meeting` (`supervisor`);

CREATE INDEX `idx_meeting__team` ON `meeting` (`team`);

ALTER TABLE `meeting` ADD CONSTRAINT `fk_meeting__supervisor` FOREIGN KEY (`supervisor`) REFERENCES `supervisor` (`id`);

ALTER TABLE `meeting` ADD CONSTRAINT `fk_meeting__team` FOREIGN KEY (`team`) REFERENCES `team` (`id`);

CREATE TABLE `actionpoint_meeting` (
  `actionpoint` INTEGER NOT NULL,
  `meeting` INTEGER NOT NULL,
  PRIMARY KEY (`actionpoint`, `meeting`)
);

CREATE INDEX `idx_actionpoint_meeting` ON `actionpoint_meeting` (`meeting`);

ALTER TABLE `actionpoint_meeting` ADD CONSTRAINT `fk_actionpoint_meeting__actionpoint` FOREIGN KEY (`actionpoint`) REFERENCES `actionpoint` (`id`);

ALTER TABLE `actionpoint_meeting` ADD CONSTRAINT `fk_actionpoint_meeting__meeting` FOREIGN KEY (`meeting`) REFERENCES `meeting` (`id`);

CREATE TABLE `agendaitem_meeting` (
  `agendaitem` INTEGER NOT NULL,
  `meeting` INTEGER NOT NULL,
  PRIMARY KEY (`agendaitem`, `meeting`)
);

CREATE INDEX `idx_agendaitem_meeting` ON `agendaitem_meeting` (`meeting`);

ALTER TABLE `agendaitem_meeting` ADD CONSTRAINT `fk_agendaitem_meeting__agendaitem` FOREIGN KEY (`agendaitem`) REFERENCES `agendaitem` (`id`);

ALTER TABLE `agendaitem_meeting` ADD CONSTRAINT `fk_agendaitem_meeting__meeting` FOREIGN KEY (`meeting`) REFERENCES `meeting` (`id`);

CREATE TABLE `student` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `user` INTEGER NOT NULL
);

CREATE INDEX `idx_student__user` ON `student` (`user`);

ALTER TABLE `student` ADD CONSTRAINT `fk_student__user` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

CREATE TABLE `meeting_student` (
  `meeting` INTEGER NOT NULL,
  `student` INTEGER NOT NULL,
  PRIMARY KEY (`meeting`, `student`)
);

CREATE INDEX `idx_meeting_student` ON `meeting_student` (`student`);

ALTER TABLE `meeting_student` ADD CONSTRAINT `fk_meeting_student__meeting` FOREIGN KEY (`meeting`) REFERENCES `meeting` (`id`);

ALTER TABLE `meeting_student` ADD CONSTRAINT `fk_meeting_student__student` FOREIGN KEY (`student`) REFERENCES `student` (`id`);

CREATE TABLE `peerassessment` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `duedate` DATE NOT NULL,
  `submitteddate` DATE NOT NULL,
  `team` INTEGER NOT NULL,
  `student` INTEGER NOT NULL
);

CREATE INDEX `idx_peerassessment__student` ON `peerassessment` (`student`);

CREATE INDEX `idx_peerassessment__team` ON `peerassessment` (`team`);

ALTER TABLE `peerassessment` ADD CONSTRAINT `fk_peerassessment__student` FOREIGN KEY (`student`) REFERENCES `student` (`id`);

ALTER TABLE `peerassessment` ADD CONSTRAINT `fk_peerassessment__team` FOREIGN KEY (`team`) REFERENCES `team` (`id`);

CREATE TABLE `student_task` (
  `student` INTEGER NOT NULL,
  `task` INTEGER NOT NULL,
  `timetaken` INTEGER NOT NULL,
  PRIMARY KEY (`student`, `task`)
);

CREATE INDEX `idx_student_task` ON `student_task` (`task`);

ALTER TABLE `student_task` ADD CONSTRAINT `fk_student_task__student` FOREIGN KEY (`student`) REFERENCES `student` (`id`);

ALTER TABLE `student_task` ADD CONSTRAINT `fk_student_task__task` FOREIGN KEY (`task`) REFERENCES `task` (`id`);

ALTER TABLE `tcab_db`.`student_task` 
ADD COLUMN `date` DATE NOT NULL AFTER `timetaken`,
ADD COLUMN `week` INT NOT NULL AFTER `date`,
ADD COLUMN `team` INT NOT NULL AFTER `week`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`student`, `task`, `team`),
ADD INDEX `fk_student_task_team_idx` (`team` ASC);
ALTER TABLE `tcab_db`.`student_task` 
ADD CONSTRAINT `fk_student_task_team`
  FOREIGN KEY (`team`)
  REFERENCES `tcab_db`.`team` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `student_unit` (
  `student` INTEGER NOT NULL,
  `unit` INTEGER NOT NULL,
  PRIMARY KEY (`student`, `unit`)
);

CREATE INDEX `idx_student_unit` ON `student_unit` (`unit`);

ALTER TABLE `student_unit` ADD CONSTRAINT `fk_student_unit__student` FOREIGN KEY (`student`) REFERENCES `student` (`id`);

ALTER TABLE `student_unit` ADD CONSTRAINT `fk_student_unit__unit` FOREIGN KEY (`unit`) REFERENCES `unit` (`id`);

CREATE TABLE `userrole` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL
);

CREATE TABLE `user_userrole` (
  `user` INTEGER NOT NULL,
  `userrole` INTEGER NOT NULL,
  PRIMARY KEY (`user`, `userrole`)
);

CREATE INDEX `idx_user_userrole` ON `user_userrole` (`userrole`);

ALTER TABLE `user_userrole` ADD CONSTRAINT `fk_user_userrole__user` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

ALTER TABLE `user_userrole` ADD CONSTRAINT `fk_user_userrole__userrole` FOREIGN KEY (`userrole`) REFERENCES `userrole` (`id`)