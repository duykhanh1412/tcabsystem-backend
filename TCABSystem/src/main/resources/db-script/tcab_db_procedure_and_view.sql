-- Search User Store Procedured
CREATE DEFINER=`root`@`localhost` PROCEDURE `searchUser`(IN firstName varchar(45),
    IN email NVARCHAR(45), IN userType INT)
BEGIN
	SET @queryTest = 'SELECT DISTINCT u.* FROM user AS u INNER JOIN user_userrole AS ur ON u.id = ur.user 
					  WHERE 1 = 1 ';
        IF (firstName != '') THEN
			SET @queryTest = CONCAT(@queryTest, 'AND u.firstname ', 'LIKE \'%', firstName, '%\' ');
		END IF;
		IF (email != '') THEN
			SET @queryTest = CONCAT(@queryTest, 'AND u.email ', 'LIKE \'%', email, '%\' ');
        END IF;
        IF (userType = 1) THEN
			SET @queryTest = CONCAT(@queryTest, 'AND (ur.userrole = 1 OR ur.userrole = 2 OR ur.userrole = 3) ');
		END IF;
        IF (userType = 4) THEN
			SET @queryTest = CONCAT(@queryTest, 'AND ur.userrole = 4 ');
		END IF;
        SET @queryTest = CONCAT(@queryTest, 'ORDER BY u.id');
        PREPARE stmt FROM @queryTest;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
END;

-- Search Project Store Procedured
CREATE DEFINER=`root`@`localhost` PROCEDURE `searchProject`(IN projectName varchar(45),
    IN unitId INT)
BEGIN
	SET @queryTest = 'SELECT DISTINCT p.id, p.name, p.maximunmembers, p.unit, u.unitcode, u.name AS unitName FROM project AS p INNER JOIN unit AS u ON p.unit = u.id WHERE (CURDATE() BETWEEN p.startdate AND p.enddate) AND 1 = 1 ';
        IF (projectName != '') THEN
			SET @queryTest = CONCAT(@queryTest, 'AND p.name ', 'LIKE \'%', projectName, '%\' ');
		END IF;
        IF (unitId > 0) THEN
			SET @queryTest = CONCAT(@queryTest, 'AND p.unit = ', unitId, ' ');
		END IF;
        SET @queryTest = CONCAT(@queryTest, 'ORDER BY p.unit');
        PREPARE stmt FROM @queryTest;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
END

-- View Project Allocation
CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `projectteamallocation` AS
    SELECT 
        `t`.`id` AS `teamId`,
        `t`.`name` AS `name`,
        `t`.`project` AS `project`,
        `t`.`supervisor` AS `supervisorId`,
        `supervisor`.`supervisorName` AS `supervisorName`,
        `student`.`studentId` AS `studentId`,
        `student`.`studentName` AS `studentName`,
        `t`.`teamcount` AS `teamcount`
    FROM
        ((`tcab_db`.`team` `t`
        JOIN (SELECT 
            `ts`.`team` AS `team`,
                `s`.`id` AS `studentId`,
                CONCAT(`u`.`firstname`, ' ', `u`.`lastname`) AS `studentName`
        FROM
            ((`tcab_db`.`team_student` `ts`
        JOIN `tcab_db`.`student` `s` ON ((`ts`.`student` = `s`.`id`)))
        JOIN `tcab_db`.`user` `u` ON ((`s`.`user` = `u`.`id`)))) `student` ON ((`t`.`id` = `student`.`team`)))
        JOIN (SELECT 
            `su`.`id` AS `supervisorId`,
                CONCAT(`u`.`firstname`, ' ', `u`.`lastname`) AS `supervisorName`
        FROM
            (`tcab_db`.`supervisor` `su`
        JOIN `tcab_db`.`user` `u` ON ((`su`.`user` = `u`.`id`)))) `supervisor` ON ((`t`.`supervisor` = `supervisor`.`supervisorId`)))