-- User Roles
INSERT INTO `tcab_db`.`userrole` (`id`, `name`) VALUES ('1', 'ADMIN');
INSERT INTO `tcab_db`.`userrole` (`id`, `name`) VALUES ('2', 'CONVENER');
INSERT INTO `tcab_db`.`userrole` (`id`, `name`) VALUES ('3', 'SUPERVISOR');
INSERT INTO `tcab_db`.`userrole` (`id`, `name`) VALUES ('4', 'STUDENT');

-- Insert a user record to the user table
INSERT INTO `tcab_db`.`user` (`id`, `username`, `password`, `firstname`, `lastname`, `title`, `mobile`, `email`, `addressline1`, `addressline2`, `addressline3`, `state`, `country`, `active`, `requiredChangePassword`, `dob`) VALUES ('2', 'test', 'MTIzNDU2Nzg5', 'Robert', 'Tipping', 'Mr', '0481365355', 'rtipping@swin.edu.au', 'Unknown', 'Unknown', 'Unknown', 'VIC', 'Australia', '1', '0', '1965-03-06');

INSERT INTO `tcab_db`.`user_userrole` (`user`, `userrole`) VALUES ('2', '1');
INSERT INTO `tcab_db`.`user_userrole` (`user`, `userrole`) VALUES ('2', '2');
INSERT INTO `tcab_db`.`user_userrole` (`user`, `userrole`) VALUES ('2', '3');
INSERT INTO `tcab_db`.`user_userrole` (`user`, `userrole`) VALUES ('2', '4');

-- Insert unit records to the unit table
INSERT INTO `tcab_db`.`unit` (`unitcode`, `name`, `description`, `startdate`, `enddate`) VALUES ('INF80030', 'Database Implementation', 'Database Implementation', '2017-03-01', '2017-06-01');
INSERT INTO `tcab_db`.`unit` (`unitcode`, `name`, `description`, `startdate`, `enddate`) VALUES ('COS10005', 'Web Development', 'Web Development', '2017-03-01', '2017-06-01');
INSERT INTO `tcab_db`.`unit` (`unitcode`, `name`, `description`, `startdate`, `enddate`) VALUES ('INF80010', 'Enterprise Architecture', 'Enterprise Architecture', '2017-03-01', '2017-06-01');

-- Insert records to the role table
INSERT INTO `tcab_db`.`role` (`name`, `associatingcost`) VALUES ('Project Manager', '100');
INSERT INTO `tcab_db`.`role` (`name`, `associatingcost`) VALUES ('Developer', '80');
INSERT INTO `tcab_db`.`role` (`name`, `associatingcost`) VALUES ('Business Analyst', '90');
INSERT INTO `tcab_db`.`role` (`name`, `associatingcost`) VALUES ('Tester', '70');


