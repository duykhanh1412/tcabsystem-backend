package com.tcabs.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tcabs.model.Convener;

public class ConvenerRepositoryImpl implements ConvenerExtendRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Convener findConvenerByUnit(int convenerId) {
		try {
			String stringQuery = "SELECT * FROM convener AS c " + "WHERE c.id = :convenerId AND "
					+ "EXISTS (SELECT * FROM convener_unit AS cu WHERE c.id = cu.convener)";
			Query query = em.createNativeQuery(stringQuery, Convener.class);
			query.setParameter("convenerId", convenerId);
			return (Convener) query.getSingleResult();
		} catch (NoResultException ex){
			throw ex;
		}
	}
}
