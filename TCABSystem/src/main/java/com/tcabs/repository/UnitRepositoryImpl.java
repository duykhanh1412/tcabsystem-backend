package com.tcabs.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.model.Unit;

public class UnitRepositoryImpl implements UnitExtendRepository {

	@PersistenceContext
	private EntityManager em;
	
	
	/**
	 * Insert new unit in to the database
	 */
	@Override
	@Modifying
	@Transactional
	public Unit saveUnit(Unit unit) {
		try {
			String saveUnit = "INSERT INTO unit (unitcode, name, description, startdate, enddate) "
					+ "VALUES (:unitcode, :name, :description, :startdate, :enddate)";
			Query saveUserQuery = em.createNativeQuery(saveUnit);
			saveUserQuery.setParameter("unitcode", unit.getUnitcode());
			saveUserQuery.setParameter("name", unit.getName());
			saveUserQuery.setParameter("description", unit.getDescription());
			saveUserQuery.setParameter("startdate", unit.getStartdate());
			saveUserQuery.setParameter("enddate", unit.getEnddate());
			saveUserQuery.executeUpdate();

			return unit;

		} catch (Exception e) {
			throw e;
		}
	}


	/**
	 * Update unit record in to the database
	 * 
	 */
	@Override
	@Modifying
	@Transactional
	public Unit updateUnit(Unit unit) {
		try {
			String updateUnit = "UPDATE unit AS u SET u.name = :name, u.description = :description,"
					+ "u.startdate = :startdate, u.enddate = :enddate "
					+ "WHERE u.id = :id";
			Query updateUnitQuery = em.createNativeQuery(updateUnit);
			updateUnitQuery.setParameter("id", unit.getId());
			updateUnitQuery.setParameter("name", unit.getName());
			updateUnitQuery.setParameter("description", unit.getDescription());
			updateUnitQuery.setParameter("startdate", unit.getStartdate());
			updateUnitQuery.setParameter("enddate", unit.getEnddate());
			updateUnitQuery.executeUpdate();

			return unit;

		} catch (Exception e) {
			throw e;
		}

	}

	
}
