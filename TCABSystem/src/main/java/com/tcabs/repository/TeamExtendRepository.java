package com.tcabs.repository;

import java.util.List;

import com.tcabs.exception.TeamException;

public interface TeamExtendRepository {

	public List<Object[]> findTeamsByProject(int projectId) throws TeamException;
	
	public Object[] findTeamById(int teamId) throws TeamException;
}
