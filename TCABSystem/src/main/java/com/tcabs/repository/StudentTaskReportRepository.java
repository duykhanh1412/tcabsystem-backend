package com.tcabs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tcabs.model.StudentTaskReport;

public interface StudentTaskReportRepository extends JpaRepository<StudentTaskReport, Integer>, StudentTaskReportExtendRepository{

	@Query("SELECT str FROM StudentTaskReport str WHERE str.user.id = :userId AND str.team = :teamId ORDER BY str.week, str.date")
	public List<StudentTaskReport> findStudentTaskReportByUserId(@Param("userId") int userId, @Param("teamId") int teamId);
	
	@Query("SELECT str FROM StudentTaskReport str WHERE str.week = :week AND str.team = :teamId ORDER BY str.student, str.date")
	public List<StudentTaskReport> findStudentTaskReportByWeek(@Param("week") int week, @Param("teamId") int teamId);
}
