package com.tcabs.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.model.StudentTask;
import com.tcabs.model.StudentTaskPK;

public interface StudentTaskRepository extends JpaRepository<StudentTask, StudentTaskPK> {

	@Query(value = "INSERT INTO student_task VALUES(:studentId, :taskId, :timeTaken, :date, :week, :teamId)", nativeQuery = true)
	@Modifying
	@Transactional
	public void saveStudentTask(@Param("studentId") int studentId, @Param("taskId") int taskId,
			@Param("timeTaken") int timeTaken, @Param("date") Date date, @Param("week") int week,
			@Param("teamId") int teamId);
	
	public List<StudentTask> findStudentTaskByStudent_IdAndTeam_IdAndWeek(int student, int team, int week);
}
