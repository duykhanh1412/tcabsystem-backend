package com.tcabs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tcabs.model.Convener;

public interface ConvenerRepository extends JpaRepository<Convener, Integer>, ConvenerExtendRepository {

	@Query("DELETE FROM Convener c WHERE c.user.id = :userId")
	@Modifying
	public void deleteConvener(@Param("userId") int userId);

	@Query("SELECT c FROM Convener c WHERE c.user.id = :userId")
	public Convener findConvenerByUserId(@Param("userId") int userId);
}