package com.tcabs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tcabs.model.User;

public interface UserRepository extends JpaRepository<User, Integer>, UserExtendRepository {

	@Query("SELECT u FROM User u WHERE u.username = :username")
	public User findByUsername(@Param("username") String username);

	@Query("SELECT u FROM User u WHERE u.email = :email")
	public User findByEmail(@Param("email") String email);

	@Query("SELECT MAX(u.id) FROM User u")
	public int findUserMaxId();

	@Query("DELETE FROM User u WHERE u.id = :userId")
	@Modifying
	public void deleteUser(@Param("userId") int userId);
	
	@Query("SELECT u FROM User u WHERE u.id = :userId")
	public User findUserById(@Param("userId") int userId);
}