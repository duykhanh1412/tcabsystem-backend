package com.tcabs.repository;

import com.tcabs.model.Supervisor;

public interface SupervisorExtendRepository {

	public Supervisor findSupervisorByUnit(int supervisorId);
}
