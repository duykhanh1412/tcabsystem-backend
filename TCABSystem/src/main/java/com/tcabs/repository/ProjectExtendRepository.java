package com.tcabs.repository;

import java.util.List;

public interface ProjectExtendRepository {

	public List<Object[]> findProjectsByProjectNameAndUnitId(String projectName, int unitId);
	
	public List<Object[]> findProjectsByUnitId(int unitId);
}