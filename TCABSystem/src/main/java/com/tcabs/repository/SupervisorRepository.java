package com.tcabs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tcabs.model.Supervisor;

public interface SupervisorRepository extends JpaRepository<Supervisor, Integer>, SupervisorExtendRepository {

	@Query("DELETE FROM Supervisor s WHERE s.user.id = :userId")
	@Modifying
	public void deleteSupervisor(@Param("userId") int userId);
	
	@Query("SELECT s FROM Supervisor s WHERE s.user.id = :userId")
	public Supervisor findSupervisorByUserId(@Param("userId") int userId);
	
	@Query(value = "SELECT s.* FROM supervisor AS s "
			+ "INNER JOIN user AS u ON s.user = u.id "
			+ "INNER JOIN employee_unit as eu ON u.id = eu.user "
			+ "WHERE eu.unit = :unitId AND s.id = :supervisorId", nativeQuery = true)
	public Supervisor findSupervisorByUnitIdAndSupervisorId(@Param("unitId") int unitId, @Param("supervisorId") int studentId);
	
	@Query(value = "SELECT s.* FROM supervisor AS s INNER JOIN user AS u ON s.user = u.id "
			+ "WHERE (u.firstname LIKE %:name% OR u.lastname LIKE %:name%) "
			+ "AND u.id IN ("
			+ "SELECT eu.user FROM employee_unit AS eu WHERE eu.unit = :unitId)", nativeQuery = true)
	public List<Supervisor> findSupervisorsByName(@Param("name") String name, @Param("unitId") int unitId);
}