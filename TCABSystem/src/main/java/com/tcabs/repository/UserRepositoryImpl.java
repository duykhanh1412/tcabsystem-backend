package com.tcabs.repository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.model.User;
import com.tcabs.model.UserRole;

public class UserRepositoryImpl implements UserExtendRepository {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private UserRepository userRepository;

	/**
	 * Insert new user to the database first then insert to the student,
	 * supervisor, and convener tables accordingly.
	 * 
	 * The user_userrole table will also be updated at the same time in one
	 * transaction
	 */
	@Override
	@Modifying
	@Transactional
	public User saveUser(User user, HashMap<String, String> userType) {
		try {
			String saveUser = "INSERT INTO user (username, password, firstname, lastname, title, mobile, email, addressline1, "
					+ "addressline2, addressline3, state, country, active, requiredChangePassword, dob) "
					+ "VALUES (:userName, :password, :firstName, :lastName, :title, :mobile, :email, :addressline1, :addressline2, "
					+ ":addressline3, :state, :country, :active, :requiredChangePassword, :dob)";
			Query saveUserQuery = em.createNativeQuery(saveUser);
			saveUserQuery.setParameter("userName", user.getUsername());
			saveUserQuery.setParameter("password", user.getPassword());
			saveUserQuery.setParameter("firstName", user.getFirstname());
			saveUserQuery.setParameter("lastName", user.getLastname());
			saveUserQuery.setParameter("title", user.getTitle());
			saveUserQuery.setParameter("mobile", user.getMobile());
			saveUserQuery.setParameter("email", user.getEmail());
			saveUserQuery.setParameter("addressline1", user.getAddressline1());
			saveUserQuery.setParameter("addressline2", user.getAddressline2());
			saveUserQuery.setParameter("addressline3", user.getAddressline3());
			saveUserQuery.setParameter("state", user.getState());
			saveUserQuery.setParameter("country", user.getCountry());
			saveUserQuery.setParameter("active", user.isActive());
			saveUserQuery.setParameter("requiredChangePassword", user.isRequiredChangePassword());
			saveUserQuery.setParameter("dob", user.getDob());
			saveUserQuery.executeUpdate();

			String saveAuthorities = "INSERT INTO user_userrole (user, userrole) VALUES (:user, :userrole)";
			int id = userRepository.findUserMaxId();
			Query saveAuthoritiesQuery = em.createNativeQuery(saveAuthorities);
			for (UserRole userRole : user.getUserroles()) {
				saveAuthoritiesQuery.setParameter("user", id);
				saveAuthoritiesQuery.setParameter("userrole", userRole.getId());
				saveAuthoritiesQuery.executeUpdate();
			}
			String saveUserType = "";
			Query saveUserTypeQuery = null;

			if (userType.containsKey("STUDENT")) {
				saveUserType = "INSERT INTO student (user) VALUE (:user)";
				saveUserTypeQuery = em.createNativeQuery(saveUserType);
				saveUserTypeQuery.setParameter("user", id);
				saveUserTypeQuery.executeUpdate();
			}
			if (userType.containsKey("SUPERVISOR")) {
				saveUserType = "INSERT INTO supervisor (user, officelocation) VALUES (:user, :officeLocation)";
				saveUserTypeQuery = em.createNativeQuery(saveUserType);
				saveUserTypeQuery.setParameter("user", id);
				saveUserTypeQuery.setParameter("officeLocation", "EN");
				saveUserTypeQuery.executeUpdate();
			}
			if (userType.containsKey("CONVENER")) {
				saveUserType = "INSERT INTO convener (user) VALUES (:user)";
				saveUserTypeQuery = em.createNativeQuery(saveUserType);
				saveUserTypeQuery.setParameter("user", id);
				saveUserTypeQuery.executeUpdate();
			}
			return user;
		} catch (Exception ex) {
			throw ex;
		}
	}


	/**
	 * This is the method used to query the database in order to find users by
	 * user name and email
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<User> findUserByUserNameAndEmail(String name, String email, int userType) {
		List<User> users = new ArrayList<>();
		try {
			String stringQuery = "CALL searchUser(:userName,:userEmail,:userType)";
			Query query = em.createNativeQuery(stringQuery, User.class);
			query.setParameter("userName", name);
			query.setParameter("userEmail", email);
			query.setParameter("userType", userType);
			users = query.getResultList();
			if (!users.isEmpty()) {
				return users;
			} else {
				throw new NoResultException();
			}
		} catch (NoResultException ex) {
			throw ex;
		}
	}

	/**
	 * This is the method to count the number of existing admin before deleting
	 * an user
	 */
	@Override
	public int getNumberExistingAdmin() {
		int result = 0;
		try {
			String stringQuery = "SELECT COUNT(*) FROM user AS u " + "INNER JOIN user_userrole AS ur ON u.id = ur.user "
					+ "WHERE ur.userrole = 1";
			Query query = em.createNativeQuery(stringQuery);
			BigInteger intermediateResult = (BigInteger) query.getSingleResult();
			result = intermediateResult.intValue();
			return result;
		} catch (NoResultException ex) {
			throw ex;
		}
	}

	/**
	 * This is the method used to update the user
	 */
	@Override
	@Modifying
	@Transactional
	public User updateUser(User user, User oldUser, HashMap<String, String> userType) {
		try {
			int id = user.getId();
			HashMap<String, String> oldUserType = new HashMap<>();
			for (UserRole userRole : oldUser.getUserroles()) {
				oldUserType.put(userRole.getName(), userRole.getName());
			}
			String saveUser = "UPDATE user AS u SET u.username = :userName, u.password = :password, u.firstname = :firstName, "
					+ "u.lastname = :lastName, u.title = :title, u.mobile = :mobile, u.email = :email, u.addressline1 = :addressline1, "
					+ "u.addressline2 = :addressline2, u.addressline3 = :addressline3, u.state = :state, u.country = :country, "
					+ "u.active = :active, u.requiredChangePassword = :requiredChangePassword, u.dob = :dob "
					+ "WHERE u.id = :id";
			Query saveUserQuery = em.createNativeQuery(saveUser);
			saveUserQuery.setParameter("id", user.getId());
			saveUserQuery.setParameter("userName", user.getUsername());
			saveUserQuery.setParameter("password", user.getPassword());
			saveUserQuery.setParameter("firstName", user.getFirstname());
			saveUserQuery.setParameter("lastName", user.getLastname());
			saveUserQuery.setParameter("title", user.getTitle());
			saveUserQuery.setParameter("mobile", user.getMobile());
			saveUserQuery.setParameter("email", user.getEmail());
			saveUserQuery.setParameter("addressline1", user.getAddressline1());
			saveUserQuery.setParameter("addressline2", user.getAddressline2());
			saveUserQuery.setParameter("addressline3", user.getAddressline3());
			saveUserQuery.setParameter("state", user.getState());
			saveUserQuery.setParameter("country", user.getCountry());
			saveUserQuery.setParameter("active", user.isActive());
			saveUserQuery.setParameter("requiredChangePassword", user.isRequiredChangePassword());
			saveUserQuery.setParameter("dob", user.getDob());
			saveUserQuery.executeUpdate();

			String deleteAuthorities = "DELETE FROM user_userrole WHERE user = :user AND userrole = :userrole";
			Query deleteAuthoritiesQuery = null;
			String deleteUserType = "";
			Query deleteUserTypeQuery = null;
			String saveUserType = "";
			Query saveUserTypeQuery = null;
			String saveAuthorities = "INSERT INTO user_userrole (user, userrole) VALUES (:user, :userrole)";
			Query saveAuthoritiesQuery = null;

			if (!userType.containsKey("STUDENT")) {
				deleteUserType = "DELETE FROM student WHERE user = :user";
				deleteUserTypeQuery = em.createNativeQuery(deleteUserType);
				deleteUserTypeQuery.setParameter("user", user.getId());
				deleteUserTypeQuery.executeUpdate();

				deleteAuthoritiesQuery = em.createNativeQuery(deleteAuthorities);
				deleteAuthoritiesQuery.setParameter("user", user.getId());
				deleteAuthoritiesQuery.setParameter("userrole", 4);
				deleteAuthoritiesQuery.executeUpdate();
			}
			if (!userType.containsKey("SUPERVISOR")) {
				deleteUserType = "DELETE FROM supervisor WHERE user = :user";
				deleteUserTypeQuery = em.createNativeQuery(deleteUserType);
				deleteUserTypeQuery.setParameter("user", user.getId());
				deleteUserTypeQuery.executeUpdate();

				deleteAuthoritiesQuery = em.createNativeQuery(deleteAuthorities);
				deleteAuthoritiesQuery.setParameter("user", user.getId());
				deleteAuthoritiesQuery.setParameter("userrole", 3);
				deleteAuthoritiesQuery.executeUpdate();
			} else if (!oldUserType.containsKey("SUPERVISOR")) {
				saveUserType = "INSERT INTO supervisor (user, officelocation) VALUES (:user, :officeLocation)";
				saveUserTypeQuery = em.createNativeQuery(saveUserType);
				saveUserTypeQuery.setParameter("user", id);
				saveUserTypeQuery.setParameter("officeLocation", "EN");
				saveUserTypeQuery.executeUpdate();

				saveAuthoritiesQuery = em.createNativeQuery(saveAuthorities);
				saveAuthoritiesQuery.setParameter("user", id);
				saveAuthoritiesQuery.setParameter("userrole", 3);
				saveAuthoritiesQuery.executeUpdate();
			}
			if (!userType.containsKey("CONVENER")) {
				deleteUserType = "DELETE FROM convener WHERE user = :user";
				deleteUserTypeQuery = em.createNativeQuery(deleteUserType);
				deleteUserTypeQuery.setParameter("user", user.getId());
				deleteUserTypeQuery.executeUpdate();

				deleteAuthoritiesQuery = em.createNativeQuery(deleteAuthorities);
				deleteAuthoritiesQuery.setParameter("user", user.getId());
				deleteAuthoritiesQuery.setParameter("userrole", 2);
				deleteAuthoritiesQuery.executeUpdate();
			} else if (!oldUserType.containsKey("CONVENER")) {
				saveUserType = "INSERT INTO convener (user) VALUES (:user)";
				saveUserTypeQuery = em.createNativeQuery(saveUserType);
				saveUserTypeQuery.setParameter("user", id);
				saveUserTypeQuery.executeUpdate();

				saveAuthoritiesQuery = em.createNativeQuery(saveAuthorities);
				saveAuthoritiesQuery.setParameter("user", id);
				saveAuthoritiesQuery.setParameter("userrole", 2);
				saveAuthoritiesQuery.executeUpdate();
			}

			if (!userType.containsKey("ADMIN")) {
				deleteAuthoritiesQuery = em.createNativeQuery(deleteAuthorities);
				deleteAuthoritiesQuery.setParameter("user", user.getId());
				deleteAuthoritiesQuery.setParameter("userrole", 1);
				deleteAuthoritiesQuery.executeUpdate();
			} else if (!oldUserType.containsKey("ADMIN")) {
				saveAuthoritiesQuery = em.createNativeQuery(saveAuthorities);
				saveAuthoritiesQuery.setParameter("user", id);
				saveAuthoritiesQuery.setParameter("userrole", 1);
				saveAuthoritiesQuery.executeUpdate();
			}
			return user;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}


	@Override
	public List<User> saveBatchUser(List<User> users, String userType) {
		List<User> userList = new ArrayList<>();
		return userList;
	}
	
	
}
