package com.tcabs.repository;

import com.tcabs.model.Convener;

public interface ConvenerExtendRepository {

	public Convener findConvenerByUnit(int convenerId);
}
