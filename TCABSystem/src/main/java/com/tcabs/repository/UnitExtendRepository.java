package com.tcabs.repository;

import com.tcabs.model.Unit;

public interface UnitExtendRepository {

	public Unit saveUnit(Unit unit);
	
	public Unit updateUnit(Unit unit);

}