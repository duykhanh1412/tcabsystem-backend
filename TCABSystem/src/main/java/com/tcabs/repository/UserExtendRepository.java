package com.tcabs.repository;

import java.util.HashMap;
import java.util.List;

import com.tcabs.model.User;

public interface UserExtendRepository {

	public User saveUser(User user, HashMap<String, String> userType);
	
	public List<User> saveBatchUser(List<User> users, String userType);
	
	public List<User> findUserByUserNameAndEmail(String name, String email, int userType);
	
	public int getNumberExistingAdmin();
	
	public User updateUser(User user, User oldUser, HashMap<String, String> userType);
}