package com.tcabs.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.model.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {

	@Query("DELETE FROM Student s WHERE s.user.id = :userId")
	@Modifying
	public void deleteStudent(@Param("userId") int userId);

	@Query("SELECT s FROM Student s INNER JOIN s.units AS unit WHERE s.id = :studentId AND unit.id = :unitId")
	public Student findStudentByUnitIdAndStudentId(@Param("unitId") int unitId, @Param("studentId") int studentId);

	@Query(value = "SELECT s.* FROM student AS s INNER JOIN user AS u ON s.user = u.id "
			+ "WHERE (u.firstname LIKE %:name% OR u.lastname LIKE %:name%) " + "AND s.id IN ("
			+ "SELECT su.student FROM student_unit AS su WHERE su.unit = :unitId)", nativeQuery = true)
	public List<Student> findStudentsByName(@Param("name") String name, @Param("unitId") int unitId);

	@Query("SELECT s FROM Student s WHERE s.user.id = :userId")
	public Student findStudentByUserId(@Param("userId") int userId);
	
	@Query(value = "INSERT INTO student_unit VALUES(:studentId, :unitId)", nativeQuery = true)
	@Modifying
	@Transactional
	public void saveStudentUnit(@Param("studentId") int studentId, @Param("unitId") int unitId);

	@Query(value = "SELECT s.* FROM student AS s INNER JOIN user AS u ON s.user = u.id "
			+ "WHERE (u.firstname LIKE %:name% OR u.lastname LIKE %:name%) " + "AND s.id IN ("
			+ "SELECT ts.student FROM team_student AS ts INNER JOIN team AS t ON ts.team = t.id "
			+ "WHERE t.id = :teamId AND t.project = :projectId)", nativeQuery = true)
	public List<Student> findStudentsByNameAndTeamIdAndProjectId(@Param("name") String name,
			@Param("teamId") int teamId, @Param("projectId") int projectId);
	
	
}
