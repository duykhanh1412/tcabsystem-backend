package com.tcabs.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tcabs.model.Supervisor;

public class SupervisorRepositoryImpl implements SupervisorExtendRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Supervisor findSupervisorByUnit(int supervisorId) {
		try {
			String stringQuery = "SELECT * FROM supervisor AS s " + "WHERE s.id = :supervisorId AND "
					+ "EXISTS (SELECT * FROM supervisor_unit AS su " + "WHERE s.id = su.supervisor)";
			Query query = em.createNativeQuery(stringQuery, Supervisor.class);
			query.setParameter("supervisorId", supervisorId);
			return (Supervisor) query.getSingleResult();
		} catch (NoResultException ex) {
			throw ex;
		}
	}
}
