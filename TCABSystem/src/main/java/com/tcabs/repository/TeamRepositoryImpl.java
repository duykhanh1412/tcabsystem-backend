package com.tcabs.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.tcabs.exception.TeamException;
import com.tcabs.model.Team;

public class TeamRepositoryImpl implements TeamExtendRepository {

	@Autowired
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findTeamsByProject(int projectId) throws TeamException {
		try {
			String stringQuery = "SELECT * FROM projectteamallocation WHERE project = :projectId";
			Query query = em.createNativeQuery(stringQuery);
			query.setParameter("projectId", projectId);
			List<Object[]> results = query.getResultList();
			if(results.isEmpty()){
				throw new TeamException(new ArrayList<Team>());
			} else {
				return results;
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public Object[] findTeamById(int teamId) throws TeamException {
		try {
			String stringQuery = "SELECT t.*, p.name AS projectName, p.maximunmembers FROM team AS t "
					+ "INNER JOIN project AS p ON t.project = p.id WHERE t.id = :teamId";
			Query query = em.createNativeQuery(stringQuery);
			query.setParameter("teamId", teamId);
			Object[] team = (Object[]) query.getSingleResult();
			if(team == null) {
				throw new TeamException(new Team());
			} else {
				return team;
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
