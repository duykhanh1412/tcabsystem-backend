package com.tcabs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import com.tcabs.model.UploadError;

public interface UploadErrorRepository extends JpaRepository<UploadError, Integer> {
	
	@Query(value = "INSERT INTO upload_error(type, name, data) "
			+ "VALUES(:type, :name, :data)", nativeQuery = true)
	@Modifying
	@Transactional
	public void saveEnrollStudentsIntoUnitErrors(@Param("type") String type, 
			@Param("name") String name, @Param("data") byte[] data);
	
	@Query(value = "SELECT u.* FROM upload_error AS u "
			+ "WHERE u.name LIKE %:name%", nativeQuery = true)
	public UploadError findUploadErrorFile(@Param("name") String name);
}
