package com.tcabs.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class ProjectRepositoryImpl implements ProjectExtendRepository {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findProjectsByProjectNameAndUnitId(String projectName, int unitId) {
		List<Object[]> projects = new ArrayList<>();
		try {
			String stringQuery = "CALL searchProject(:projectName, :unitId)";
			Query query = em.createNativeQuery(stringQuery);
			query.setParameter("projectName", projectName);
			query.setParameter("unitId", unitId);
			projects = query.getResultList();
			if (!projects.isEmpty()) {
				return projects;
			} else {
				throw new NoResultException();
			}
		} catch (NoResultException ex) {
			throw ex;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findProjectsByUnitId(int unitId) {
		List<Object[]> projects = new ArrayList<>();
		try {
			String stringQuery = "SELECT p.id, p.name, p.maximunmembers, p.budget FROM project AS p "
					+ "WHERE p.unit = :unitId AND CURDATE() BETWEEN p.startdate AND p.enddate";
			Query query = em.createNativeQuery(stringQuery);
			query.setParameter("unitId", unitId);
			projects = query.getResultList();
			if (!projects.isEmpty()) {
				return projects;
			} else {
				throw new NoResultException();
			}
		} catch (NoResultException ex) {
			throw ex;
		}
	}

}
