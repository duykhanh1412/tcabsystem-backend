package com.tcabs.repository;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.model.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer>, ProjectExtendRepository {

	@Query(value = "INSERT INTO project(name, description, maximunmembers, unit, "
			+ "descriptionFileType, descriptionFileName, startdate, enddate, budget) "
			+ "VALUES(:name, :description, :maximumNumber, :unitId, :descriptionFileType, "
			+ ":descriptionFileName, :startDate, :endDate, :budget)", nativeQuery = true)
	@Modifying
	@Transactional
	public void saveProject(@Param("name") String name, @Param("description") byte[] description,
			@Param("maximumNumber") int maximumNumber, @Param("unitId") int unitId,
			@Param("descriptionFileType") String descriptionFileType,
			@Param("descriptionFileName") String descriptionFileName, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate, @Param("budget") float budget);

	@Query("SELECT p FROM Project p WHERE p.name = :projectName AND p.unit.id = :unitId AND CURDATE() BETWEEN :startdate AND :enddate")
	public Project findProjectByProjectNameAndUnitCode(@Param("projectName") String projectName,
			@Param("unitId") int unitId, @Param("startdate") Date startdate, @Param("enddate") Date enddate);

	@Query("DELETE FROM Project p WHERE p.id = :projectId")
	@Modifying
	@Transactional
	public void deleteProject(@Param("projectId") int projectId);

	@Query("SELECT p FROM Project p WHERE p.id = :projectId")
	public Project findProjectById(@Param("projectId") int projectId);

	@Query(value = "UPDATE project SET project.name = :name, project.description = :description, "
			+ "project.maximunmembers = :maximumNumber, project.unit = :unitId, project.descriptionFileType = :descriptionFileType, "
			+ "project.descriptionFileName = :descriptionFileName, project.budget = :budget "
			+ "WHERE project.id = :projectId", nativeQuery = true)
	@Modifying
	@Transactional
	public void updateProjectWithDescription(@Param("name") String name, @Param("description") byte[] description,
			@Param("maximumNumber") int maximumNumber, @Param("unitId") int unitId,
			@Param("descriptionFileType") String descriptionFileType,
			@Param("descriptionFileName") String descriptionFileName, @Param("projectId") int projectId,
			@Param("budget") float budget);

	@Query(value = "UPDATE project SET project.name = :name, project.maximunmembers = :maximumNumber, "
			+ "project.unit = :unitId, project.budget = :budget WHERE project.id = :projectId", nativeQuery = true)
	@Modifying
	@Transactional
	public void updateProjectWithouthDescription(@Param("name") String name, @Param("maximumNumber") int maximumNumber,
			@Param("unitId") int unitId, @Param("projectId") int projectId, @Param("budget") float budget);
}
