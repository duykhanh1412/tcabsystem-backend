package com.tcabs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tcabs.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	@Query(value = "SELECT r.* FROM role AS r INNER JOIN project_role AS pr ON r.id = pr.role "
			+ "WHERE pr.project = :projectId", nativeQuery = true)
	public List<Role> findProjectRolesByProjectId(@Param("projectId") int projectId);
}
