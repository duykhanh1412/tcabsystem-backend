package com.tcabs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.model.Team;

public interface TeamRepository extends JpaRepository<Team, Integer>, TeamExtendRepository {

	@Query(value = "UPDATE team SET team.project = :projectId, team.supervisor = :supervisorId WHERE team.id = :teamId", nativeQuery = true)
	@Modifying
	@Transactional
	public void allocateTeamToProject(@Param("teamId") int teamId, @Param("projectId") int projectId,
			@Param("supervisorId") int supervisorId);

	@Query("SELECT t FROM Team t WHERE t.id = :teamId")
	public Team findTeamByTeamId(@Param("teamId") int teamId);

	@Query(value = "UPDATE team SET team.project = null, team.supervisor = null WHERE team.id = :teamId", nativeQuery = true)
	@Modifying
	@Transactional
	public void updateProjectAllocation(@Param("teamId") int teamId);

	@Query("SELECT t FROM Team t WHERE t.name = :teamName AND t.project.id = :projectId")
	public Team findTeamByTeamNameAndProjectId(@Param("teamName") String teamName, @Param("projectId") int projectId);

	@Query(value = "INSERT INTO team VALUES (:teamName, :projectId, :supervisorId, :teamCount)", nativeQuery = true)
	@Modifying
	@Transactional
	public void registerTeam(@Param("teamName") String teamName, @Param("projectId") int projectId,
			@Param("supervisorId") int supervisorId, @Param("teamCount") int teamCount);

	@Query("SELECT t FROM Team t INNER JOIN t.students AS student WHERE t.project.id = :projectId AND student.user.id = :userId")
	public Team findTeamByProjectIdAndUserId(@Param("userId") int userId, @Param("projectId") int projectId);
	
	@Query("SELECT new Team(t.id, t.name, t.project.id, t.project.maximunmembers, t.project.name, t.project.unit.id, t.project.unit.unitcode, t.project.unit.name, "
			+ "t.supervisor.id, t.supervisor.user.id, t.supervisor.user.firstname, t.supervisor.user.lastname, t.teamcount) "
			+ "FROM Team t "
			+ "WHERE t.name LIKE %:teamName% AND t.project.id = :projectId")
	public List<Team> searchTeamByTeamNameAndProjectId(@Param("teamName") String teamName, @Param("projectId") int projectId);
	
	@Query("SELECT new Team(t.id, t.name, t.project.id, t.project.maximunmembers, t.project.name, t.project.unit.id, t.project.unit.unitcode, t.project.unit.name, "
			+ "t.supervisor.id, t.supervisor.user.id, t.supervisor.user.firstname, t.supervisor.user.lastname, t.teamcount, student) "
			+ "FROM Team t INNER JOIN t.students AS student "
			+ "WHERE t.id = :teamId")
	public List<Team> searchTeamByTeamId(@Param("teamId") int teamId);
	
	@Query("DELETE Team t WHERE t.id = :teamId")
	@Modifying
	@Transactional
	public void deleteTeam(@Param("teamId") int teamId);
}
