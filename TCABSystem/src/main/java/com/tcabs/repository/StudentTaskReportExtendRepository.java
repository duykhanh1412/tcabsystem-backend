package com.tcabs.repository;

import java.util.List;

public interface StudentTaskReportExtendRepository {

	public List<Object[]> findTeamTaskReportByTeamId(int teamId) throws Exception;
}
