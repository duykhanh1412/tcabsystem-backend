package com.tcabs.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.tcabs.exception.StudentTaskReportException;

public class StudentTaskReportRepositoryImpl implements StudentTaskReportExtendRepository{

	@Autowired
	private EntityManager em;
	@Override
	public List<Object[]> findTeamTaskReportByTeamId(int teamId) throws Exception {
		try {
			String stringQuery = "SELECT st.student, st.user, CONCAT(u.firstname, ' ', u.lastname) AS name, st.week, "
					+ "SUM(st.timetaken) AS totalHourPerWeek, SUM(totalCost) AS totalCostPerWeek "
					+ "FROM student_task_report AS st INNER JOIN user AS u ON st.user = u.id "
					+ "WHERE st.team = :teamId GROUP BY st.student, st.user, st.week";
			Query query = em.createNativeQuery(stringQuery);
			query.setParameter("teamId", teamId);
			@SuppressWarnings("unchecked")
			List<Object[]> results = query.getResultList();
			if(results.isEmpty()){
				throw new StudentTaskReportException(new ArrayList<>());
			} else {
				return results;
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

}
