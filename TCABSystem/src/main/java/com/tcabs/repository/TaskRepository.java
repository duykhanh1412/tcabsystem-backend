package com.tcabs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.model.Task;

public interface TaskRepository extends JpaRepository<Task, Integer> {

	@Query(value = "INSERT INTO task(deacription, role) VALUES(:description, :roleId)", nativeQuery = true)
	@Modifying
	@Transactional
	public void saveTask(@Param("description") String description, @Param("roleId") int roleId);
	
	@Query("SELECT MAX(t.id) FROM Task t")
	public int getMaximumTaskId();
}
