package com.tcabs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.model.Unit;

public interface UnitRepository extends JpaRepository<Unit, Integer>, UnitExtendRepository {

	@Query("SELECT u FROM Unit u WHERE CURDATE() BETWEEN u.startdate AND u.enddate")
	public List<Unit> findAllCurrentUnit();

	@Query("DELETE Unit u WHERE u.id = :unitId")
	@Modifying
	@Transactional
	public void deleteUnit(@Param("unitId") int unitId);

	@Query(value = "SELECT u.* FROM unit AS u " + "WHERE (u.name LIKE %:name%) "
			+ "AND CURDATE() BETWEEN u.startdate AND u.enddate", nativeQuery = true)
	public List<Unit> findUnitsByName(@Param("name") String name);

	@Query(value = "SELECT u.* FROM unit AS u " + "WHERE (u.unitCode LIKE %:unitCode%) "
			+ "AND CURDATE() BETWEEN u.startdate AND u.enddate", nativeQuery = true)
	public List<Unit> findUnitsByUnitCode(@Param("unitCode") String unitCode);

	@Query(value = "SELECT u.* FROM unit AS u " + "WHERE (u.name LIKE %:name%) " + "AND (u.unitCode LIKE %:unitCode%) "
			+ "AND CURDATE() BETWEEN u.startdate AND u.enddate", nativeQuery = true)
	public List<Unit> searchUnits(@Param("name") String name, @Param("unitCode") String unitCode);

	@Query("SELECT u FROM Unit u WHERE u.id = :unitId")
	public Unit findUnitsById(@Param("unitId") int unitId);

	@Query(value = "SELECT DISTINCT u.* FROM unit AS u INNER JOIN employee_unit AS eu ON u.id = eu.unit "
			+ "INNER JOIN convener AS c ON eu.user = c.user "
			+ "WHERE (CURDATE() BETWEEN u.startdate AND u.enddate) AND c.user = :userId", nativeQuery = true)
	public List<Unit> findAllCurrentUnitOfConvener(@Param("userId") int userId);

	@Query(value = "SELECT DISTINCT u.* FROM unit AS u INNER JOIN student_unit AS su ON u.id = su.unit "
			+ "INNER JOIN student AS s ON su.student = s.id " + "INNER JOIN user AS us ON s.user = us.id "
			+ "WHERE (CURDATE() BETWEEN u.startdate AND u.enddate) AND us.id = :userId", nativeQuery = true)
	public List<Unit> findAllCurrentUnitOfStudent(@Param("userId") int userId);
}
