package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

//import java.util.Collection;

import java.util.List;

/**
 * The persistent class for the userrole database table.
 * 
 */
@Entity
//@NamedQuery(name = "Userrole.findAll", query = "SELECT u FROM Userrole u")
public class UserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	private String name;

	// bi-directional many-to-many association to User
	@ManyToMany(mappedBy = "userroles", fetch = FetchType.EAGER)
//	@JoinTable(name = "user_userrole", joinColumns = { @JoinColumn(name = "userrole") }, inverseJoinColumns = {
//			@JoinColumn(name = "user") })
	private List<User> users;

	public UserRole() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public List<User> getUsers() {
		return this.users;
	}

	@JsonIgnore
	public void setUsers(List<User> users) {
		this.users = users;
	}

}