package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the meeting database table.
 * 
 */
@Entity
//@NamedQuery(name = "Meeting.findAll", query = "SELECT m FROM Meeting m")
public class Meeting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Size(min = 2, max = 45)
	private String location;

	@Lob
	private byte[] munites;

	@NotNull
	@Size(min = 2, max = 45)
	private String supervisorcomments;

	@Temporal(TemporalType.TIMESTAMP)
	private Date time;

	private String type;

	// bi-directional many-to-one association to Supervisor
	@ManyToOne
	@JoinColumn(name = "supervisor")
	private Supervisor supervisor;

	// bi-directional many-to-one association to Team
	@ManyToOne
	@JoinColumn(name = "team")
	private Team team;

	// bi-directional many-to-many association to Student
	@ManyToMany
	@JoinTable(name = "meeting_student", joinColumns = { @JoinColumn(name = "meeting") }, inverseJoinColumns = {
			@JoinColumn(name = "student") })
	private List<Student> students;

	public Meeting() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public byte[] getMunites() {
		return this.munites;
	}

	public void setMunites(byte[] munites) {
		this.munites = munites;
	}

	public String getSupervisorcomments() {
		return this.supervisorcomments;
	}

	public void setSupervisorcomments(String supervisorcomments) {
		this.supervisorcomments = supervisorcomments;
	}

	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Supervisor getSupervisor() {
		return this.supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public List<Student> getStudents() {
		return this.students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

}