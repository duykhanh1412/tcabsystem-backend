package com.tcabs.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

/**
 * The persistent class for the project database table.
 * 
 */
@Entity
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] description;

	@NotNull
	private int maximunmembers;

	private String descriptionFileType;

	private String descriptionFileName;
	
	@NotNull
	private Date startDate;
	
	@NotNull
	private Date endDate;

	@NotNull
	private String name;

	@NotNull
	private float budget;
	
	// bi-directional many-to-one association to Unit
	@ManyToOne
	@JoinColumn(name = "unit")
	private Unit unit;

	// bi-directional many-to-one association to Team
	@OneToMany(mappedBy = "project")
	private List<Team> teams;

	// bi-directional many-to-many association to Role
	@ManyToMany
	@JoinTable(name = "project_role", joinColumns = { @JoinColumn(name = "project") }, inverseJoinColumns = {
			@JoinColumn(name = "role") })
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Role> roles;

	// bi-directional many-to-many association to Task
	@ManyToMany
	@JoinTable(name = "project_task", joinColumns = { @JoinColumn(name = "project") }, inverseJoinColumns = {
			@JoinColumn(name = "task") })
	@JsonIgnore
	private List<Task> tasks;

	public Project() {
	}

	public Project(int id, String name, int maximunmembers, int unitId, String unitCode, String unitName) {
		super();
		this.id = id;
		this.name = name;
		this.maximunmembers = maximunmembers;
		this.unit = new Unit(unitId, unitCode, unitName);
	}

	public Project(byte[] description, int maximunmembers, String name, Unit unit, String descriptionFileType,
			String descriptionFileName, Date startDate, Date endDate, float budget) {
		super();
		this.description = description;
		this.maximunmembers = maximunmembers;
		this.name = name;
		this.unit = unit;
		this.descriptionFileType = descriptionFileType;
		this.descriptionFileName = descriptionFileName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.budget = budget;
	}

	public int getId() {
		return this.id;
	}

	public Project(int id, int maximunmembers, String name, Unit unit, float budget) {
		super();
		this.id = id;
		this.maximunmembers = maximunmembers;
		this.name = name;
		this.unit = unit;
		this.budget = budget;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getDescription() {
		return this.description;
	}

	public void setDescription(byte[] description) {
		this.description = description;
	}

	public int getMaximunmembers() {
		return this.maximunmembers;
	}

	public void setMaximunmembers(int maximunmembers) {
		this.maximunmembers = maximunmembers;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@JsonIgnore
	public List<Team> getTeams() {
		return this.teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public Team addTeam(Team team) {
		getTeams().add(team);
		team.setProject(this);

		return team;
	}

	public Team removeTeam(Team team) {
		getTeams().remove(team);
		team.setProject(null);

		return team;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public String getDescriptionFileType() {
		return descriptionFileType;
	}

	public void setDescriptionFileType(String descriptionFileType) {
		this.descriptionFileType = descriptionFileType;
	}

	public String getDescriptionFileName() {
		return descriptionFileName;
	}

	public void setDescriptionFileName(String descriptionFileName) {
		this.descriptionFileName = descriptionFileName;
	}

	public boolean isDescriptionIncluded() {
		if (this.description != null) {
			return true;
		} else
			return false;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public float getBudget() {
		return budget;
	}

	public void setBudget(float budget) {
		this.budget = budget;
	}
}