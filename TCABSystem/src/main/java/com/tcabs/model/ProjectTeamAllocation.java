package com.tcabs.model;

import java.util.List;

public class ProjectTeamAllocation {

	private int teamId;
	
	private String name;
	
	private int projectId;
	
	private int supervisorId;
	
	private String supervisorName;
	
	private List<StudentInformation> students;
	
	private int maximunmembers;
	
	private int teamcount;

	public ProjectTeamAllocation() {
		super();
	}

	public ProjectTeamAllocation(int teamId, String name, int projectId, int supervisorId, String supervisorName,
			List<StudentInformation> students, int teamcount) {
		super();
		this.teamId = teamId;
		this.name = name;
		this.projectId = projectId;
		this.supervisorId = supervisorId;
		this.supervisorName = supervisorName;
		this.students = students;
		this.teamcount = teamcount;
	}

	
	public ProjectTeamAllocation(int teamId, String name, int projectId, int supervisorId, String supervisorName,
			List<StudentInformation> students, int maximunmembers, int teamcount) {
		super();
		this.teamId = teamId;
		this.name = name;
		this.projectId = projectId;
		this.supervisorId = supervisorId;
		this.supervisorName = supervisorName;
		this.students = students;
		this.maximunmembers = maximunmembers;
		this.teamcount = teamcount;
	}

	public int getMaximunmembers() {
		return maximunmembers;
	}

	public void setMaximunmembers(int maximunmembers) {
		this.maximunmembers = maximunmembers;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(int supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getSupervisorName() {
		return supervisorName;
	}

	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}

	public List<StudentInformation> getStudents() {
		return students;
	}

	public void setStudents(List<StudentInformation> students) {
		this.students = students;
	}

	public int getTeamcount() {
		return teamcount;
	}

	public void setTeamcount(int teamcount) {
		this.teamcount = teamcount;
	}

	
}
