package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

/**
 * The persistent class for the student database table.
 * 
 */
@Entity
public class Student implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	// bi-directional many-to-one association to Peerassessment
	@OneToMany(mappedBy = "student")
	@JsonIgnore
	private List<Peerassessment> peerassessments;

	// bi-directional many-to-one association to Team
	@ManyToMany(mappedBy = "students", fetch = FetchType.EAGER)
	@OrderColumn(name = "id")
	private List<Team> teams;

	// bi-directional many-to-one association to User
	@OneToOne
	@JoinColumn(name = "user")
	private User user;

	// bi-directional many-to-many association to Meeting
	@ManyToMany(mappedBy = "students")
	@JsonIgnore
	private List<Meeting> meetings;

	// bi-directional many-to-one association to StudentTask
	@OneToMany(mappedBy = "student")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<StudentTask> studentTasks;

	// bi-directional many-to-many association to Unit
	@ManyToMany
	@JoinTable(name = "student_unit", joinColumns = { @JoinColumn(name = "student") }, inverseJoinColumns = {
			@JoinColumn(name = "unit") })
	@JsonIgnore
	private List<Unit> units;

	public Student() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Peerassessment> getPeerassessments() {
		return this.peerassessments;
	}

	public void setPeerassessments(List<Peerassessment> peerassessments) {
		this.peerassessments = peerassessments;
	}

	public Peerassessment addPeerassessment(Peerassessment peerassessment) {
		getPeerassessments().add(peerassessment);
		peerassessment.setStudent(this);

		return peerassessment;
	}

	public Peerassessment removePeerassessment(Peerassessment peerassessment) {
		getPeerassessments().remove(peerassessment);
		peerassessment.setStudent(null);

		return peerassessment;
	}

	/**
	 * This annotation will get rid of the Team when the Student is fetched
	 * 
	 * @return
	 */
	@JsonBackReference
	public List<Team> getTeam() {
		return this.teams;
	}

	public void setTeam(List<Team> teams) {
		this.teams = teams;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Meeting> getMeetings() {
		return this.meetings;
	}

	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}

	public List<StudentTask> getStudentTasks() {
		return this.studentTasks;
	}

	public void setStudentTasks(List<StudentTask> studentTasks) {
		this.studentTasks = studentTasks;
	}

	public StudentTask addStudentTask(StudentTask studentTask) {
		getStudentTasks().add(studentTask);
		studentTask.setStudent(this);

		return studentTask;
	}

	public StudentTask removeStudentTask(StudentTask studentTask) {
		getStudentTasks().remove(studentTask);
		studentTask.setStudent(null);

		return studentTask;
	}

	public List<Unit> getUnits() {
		return this.units;
	}

	public void setUnits(List<Unit> units) {
		this.units = units;
	}

}