package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

/**
 * The persistent class for the role database table.
 * 
 */
@Entity
//@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	private double associatingcost;

	@NotNull
	private String name;

	// bi-directional many-to-one association to Task
	@OneToMany(mappedBy = "role")
	@JsonIgnore
	private List<Task> tasks;

	// bi-directional many-to-many association to Project
	@ManyToMany(mappedBy = "roles")
	private List<Project> projects;

	public Role() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAssociatingcost() {
		return this.associatingcost;
	}

	public void setAssociatingcost(double associatingcost) {
		this.associatingcost = associatingcost;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public Task addTask(Task task) {
		getTasks().add(task);
		task.setRole(this);

		return task;
	}

	public Task removeTask(Task task) {
		getTasks().remove(task);
		task.setRole(null);

		return task;
	}

	@JsonBackReference
	public List<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

}