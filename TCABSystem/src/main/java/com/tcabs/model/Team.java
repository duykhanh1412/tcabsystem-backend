package com.tcabs.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the team database table.
 * 
 */
@Entity
public class Team implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	private String name;

	private int teamcount;

	// bi-directional many-to-one association to Meeting
	@OneToMany(mappedBy = "team")
	@JsonIgnore
	private List<Meeting> meetings;

	// bi-directional many-to-one association to Peerassessment
	@OneToMany(mappedBy = "team")
	@JsonIgnore
	private List<Peerassessment> peerassessments;

	/**
	 * bi-directional many-to-many association to Student
	 * 
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "team_student", joinColumns = { @JoinColumn(name = "team") }, inverseJoinColumns = {
			@JoinColumn(name = "student") })
	private List<Student> students;

	// bi-directional many-to-one association to Project
	@ManyToOne
	@JoinColumn(name = "project")
	private Project project;

	// bi-directional many-to-one association to Supervisor
	@ManyToOne
	@JoinColumn(name = "supervisor")
	private Supervisor supervisor;

	// bi-directional many-to-one association to StudentTask
	@OneToMany(mappedBy = "team")
	private List<StudentTask> studentTasks;

	@Transient
	private Student student;

	public Team() {
	}

	public Team(List<Student> students) {
		super();
		this.students = students;
	}

	public Team(int id, String name, int projectId, int maximunmembers, String projectName, int unitId, String unitCode,
			String unitName, int supervisorId, int userId, String firstname, String lastname, int teamcount,
			Student student) {
		super();
		this.id = id;
		this.name = name;
		this.project = new Project(projectId, projectName, maximunmembers, unitId, unitCode, unitName);
		this.supervisor = new Supervisor(supervisorId, userId, firstname, lastname);
		this.teamcount = teamcount;
		this.student = student;
	}

	public Team(int id, String name, int projectId, int maximunmembers, String projectName, int unitId, String unitCode,
			String unitName, int supervisorId, int userId, String firstname, String lastname, int teamcount) {
		super();
		this.id = id;
		this.name = name;
		this.project = new Project(projectId, projectName, maximunmembers, unitId, unitCode, unitName);
		this.supervisor = new Supervisor(supervisorId, userId, firstname, lastname);
		this.teamcount = teamcount;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Meeting> getMeetings() {
		return this.meetings;
	}

	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}

	public Meeting addMeeting(Meeting meeting) {
		getMeetings().add(meeting);
		meeting.setTeam(this);

		return meeting;
	}

	public Meeting removeMeeting(Meeting meeting) {
		getMeetings().remove(meeting);
		meeting.setTeam(null);

		return meeting;
	}

	public List<Peerassessment> getPeerassessments() {
		return this.peerassessments;
	}

	public void setPeerassessments(List<Peerassessment> peerassessments) {
		this.peerassessments = peerassessments;
	}

	public Peerassessment addPeerassessment(Peerassessment peerassessment) {
		getPeerassessments().add(peerassessment);
		peerassessment.setTeam(this);

		return peerassessment;
	}

	public Peerassessment removePeerassessment(Peerassessment peerassessment) {
		getPeerassessments().remove(peerassessment);
		peerassessment.setTeam(null);

		return peerassessment;
	}

	public List<Student> getStudents() {
		return this.students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Supervisor getSupervisor() {
		return this.supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}

	public int getTeamcount() {
		return teamcount;
	}

	public void setTeamcount(int teamcount) {
		this.teamcount = teamcount;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public boolean isTeamValid() {
		return this.id > 0 ? true : false;
	}

	@JsonBackReference
	public List<StudentTask> getStudentTasks() {
		return studentTasks;
	}

	public void setStudentTasks(List<StudentTask> studentTasks) {
		this.studentTasks = studentTasks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		if (id != other.id)
			return false;
		return true;
	}
}