package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The persistent class for the agendaitem database table.
 * 
 */
@Entity
//@NamedQuery(name = "Agendaitem.findAll", query = "SELECT a FROM Agendaitem a")
public class Agendaitem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Size(max = 45)
	private String description;

	@NotNull
	@Size(min = 2, max = 45)
	private String name;

	public Agendaitem() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}