package com.tcabs.model;

public class Response {

	private String reason;
	
	private String message;

	public Response() {
		super();
	}

	public Response(String reason, String message) {
		super();
		this.reason = reason;
		this.message = message;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
