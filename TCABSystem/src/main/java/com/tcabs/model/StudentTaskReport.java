package com.tcabs.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "student_task_report")
public class StudentTaskReport {

	@Id
	private int task;
	
	private int student;
	
	private int team;
	
	private Date date;
	
	private int week;
	
	private String taskDescription;
	
	private float timeTaken;
	
	private String roleName;
	
	private float associatingCost;
	
	private float totalCost;
	
	@Transient
	private int numberOfWeek;
	
	@OneToOne
	@JoinColumn(name = "user")
	private User user;

	public StudentTaskReport() {
		super();
	}

	public StudentTaskReport(int task, int student, int team, Date date, int week, String taskDescription,
			float timeTaken, String roleName, float associatingCost, float totalCost, int numberOfWeek, User user) {
		super();
		this.task = task;
		this.student = student;
		this.team = team;
		this.date = date;
		this.week = week;
		this.taskDescription = taskDescription;
		this.timeTaken = timeTaken;
		this.roleName = roleName;
		this.associatingCost = associatingCost;
		this.totalCost = totalCost;
		this.numberOfWeek = numberOfWeek;
		this.user = user;
	}

	public StudentTaskReport(int task, int student, int team, Date date, int week, String taskDescription,
			float timeTaken, String roleName, float associatingCost, float totalCost, User user) {
		super();
		this.task = task;
		this.student = student;
		this.team = team;
		this.date = date;
		this.week = week;
		this.taskDescription = taskDescription;
		this.timeTaken = timeTaken;
		this.roleName = roleName;
		this.associatingCost = associatingCost;
		this.totalCost = totalCost;
		this.user = user;
	}

	public int getTask() {
		return task;
	}

	public void setTask(int task) {
		this.task = task;
	}

	public int getStudent() {
		return student;
	}

	public void setStudent(int student) {
		this.student = student;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public float getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(float timeTaken) {
		this.timeTaken = timeTaken;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public float getAssociatingCost() {
		return associatingCost;
	}

	public void setAssociatingCost(float associatingCost) {
		this.associatingCost = associatingCost;
	}

	public float getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getTeam() {
		return team;
	}

	public void setTeam(int team) {
		this.team = team;
	}

	public int getNumberOfWeek() {
		return numberOfWeek;
	}

	public void setNumberOfWeek(int numberOfWeek) {
		this.numberOfWeek = numberOfWeek;
	}
}
