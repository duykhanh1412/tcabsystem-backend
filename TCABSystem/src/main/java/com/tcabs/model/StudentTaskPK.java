package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the student_task database table.
 * 
 */
@Embeddable
public class StudentTaskPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "student", insertable = false, updatable = false)
	private int student;

	@Column(name = "task", insertable = false, updatable = false)
	private int task;

	@Column(name = "team", insertable = false, updatable = false)
	private int team;

	public StudentTaskPK() {
	}

	public int getStudent() {
		return this.student;
	}

	public void setStudent(int student) {
		this.student = student;
	}

	public int getTask() {
		return this.task;
	}

	public void setTask(int task) {
		this.task = task;
	}

	public int getTeam() {
		return team;
	}

	public void setTeam(int team) {
		this.team = team;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof StudentTaskPK)) {
			return false;
		}
		StudentTaskPK castOther = (StudentTaskPK) other;
		return (this.student == castOther.student) && (this.task == castOther.task) && (this.team == castOther.team);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.student;
		hash = hash * prime + this.task;
		hash = hash * prime + this.team;

		return hash;
	}
}