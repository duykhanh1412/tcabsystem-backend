package com.tcabs.model;

import java.util.List;

public class Week {

	private int week;
	
	private List<StudentWeeklyTask> studentWeeklyTask;
	
	private int toltalHourPerWeekForTeam;
	
	private float totalCostPerWeekForTeam;

	public Week() {
		super();
	}

	public Week(int week, List<StudentWeeklyTask> studentWeeklyTask, int toltalHourPerWeekForTeam,
			float totalCostPerWeekForTeam) {
		super();
		this.week = week;
		this.studentWeeklyTask = studentWeeklyTask;
		this.toltalHourPerWeekForTeam = toltalHourPerWeekForTeam;
		this.totalCostPerWeekForTeam = totalCostPerWeekForTeam;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	public List<StudentWeeklyTask> getStudentWeeklyTask() {
		return studentWeeklyTask;
	}

	public void setStudentWeeklyTask(List<StudentWeeklyTask> studentWeeklyTask) {
		this.studentWeeklyTask = studentWeeklyTask;
	}

	public int getToltalHourPerWeekForTeam() {
		return toltalHourPerWeekForTeam;
	}

	public void setToltalHourPerWeekForTeam(int toltalHourPerWeekForTeam) {
		this.toltalHourPerWeekForTeam = toltalHourPerWeekForTeam;
	}

	public float getTotalCostPerWeekForTeam() {
		return totalCostPerWeekForTeam;
	}

	public void setTotalCostPerWeekForTeam(float totalCostPerWeekForTeam) {
		this.totalCostPerWeekForTeam = totalCostPerWeekForTeam;
	}
}
