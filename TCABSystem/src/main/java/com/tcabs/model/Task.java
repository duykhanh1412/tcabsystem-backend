package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.List;

/**
 * The persistent class for the task database table.
 * 
 */
@Entity
// @NamedQuery(name="Task.findAll", query="SELECT t FROM Task t")
public class Task implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Size(max = 45)
	private String deacription;

	// bi-directional many-to-one association to Role
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "role")
	private Role role;
	//
	// // bi-directional many-to-many association to Project
	// @ManyToMany(mappedBy = "tasks")
	// private List<Project> projects;

	// bi-directional many-to-one association to StudentTask
	@OneToMany(mappedBy = "task")
	private List<StudentTask> studentTasks;

	public Task() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeacription() {
		return this.deacription;
	}

	public void setDeacription(String deacription) {
		this.deacription = deacription;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	// public List<Project> getProjects() {
	// return this.projects;
	// }
	//
	// public void setProjects(List<Project> projects) {
	// this.projects = projects;
	// }

	@JsonBackReference
	public List<StudentTask> getStudentTasks() {
		return this.studentTasks;
	}

	public void setStudentTasks(List<StudentTask> studentTasks) {
		this.studentTasks = studentTasks;
	}

	public StudentTask addStudentTask(StudentTask studentTask) {
		getStudentTasks().add(studentTask);
		studentTask.setTask(this);

		return studentTask;
	}

	public StudentTask removeStudentTask(StudentTask studentTask) {
		getStudentTasks().remove(studentTask);
		studentTask.setTask(null);

		return studentTask;
	}

}