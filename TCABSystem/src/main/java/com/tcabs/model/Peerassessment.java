package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.util.Date;

/**
 * The persistent class for the peerassessment database table.
 * 
 */
@Entity
//@NamedQuery(name = "Peerassessment.findAll", query = "SELECT p FROM Peerassessment p")
public class Peerassessment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.DATE)
	@NotNull
	private Date duedate;

	@Temporal(TemporalType.DATE)
	private Date submitteddate;

	// bi-directional many-to-one association to Student
	@ManyToOne
	@JoinColumn(name = "student")
	private Student student;

	// bi-directional many-to-one association to Team
	@ManyToOne
	@JoinColumn(name = "team")
	private Team team;

	public Peerassessment() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDuedate() {
		return this.duedate;
	}

	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

	public Date getSubmitteddate() {
		return this.submitteddate;
	}

	public void setSubmitteddate(Date submitteddate) {
		this.submitteddate = submitteddate;
	}

	public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}