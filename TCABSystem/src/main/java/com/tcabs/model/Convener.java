package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the convener database table.
 * 
 */
@Entity
//@NamedQuery(name = "Convener.findAll", query = "SELECT c FROM Convener c")
public class Convener implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	// bi-directional many-to-one association to User
	@OneToOne
	@JoinColumn(name = "user")
	private User user;

	public Convener() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}