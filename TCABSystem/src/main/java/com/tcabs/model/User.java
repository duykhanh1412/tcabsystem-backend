package com.tcabs.model;

import java.io.Serializable;
import java.sql.Date;

//import java.util.Collection;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * The persistent class for the user database table.
 * 
 */
@Entity
// @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private boolean active;

	@NotNull
	@Size(min = 2, max = 45)
	private String addressline1;

	@Size(max = 45)
	private String addressline2;

	@Size(max = 45)
	private String addressline3;

	@NotNull
	@Size(min = 2, max = 45)
	private String country;

	@NotNull
	@Size(min = 2, max = 45)
	private String email;

	@NotNull
	@Size(min = 2, max = 45)
	private String firstname;

	@NotNull
	@Size(min = 2, max = 45)
	private String lastname;

	@Pattern(regexp = "0[1-9]([0-9]{7,10})")
	private String mobile;

	private String password;

	@Size(max = 45)
	private String state;

	@NotNull
	@Size(min = 2, max = 45)
	private String title;

	private String username;
	
	private boolean requiredChangePassword;
	
	@NotNull
	private Date dob;

	// bi-directional many-to-many association to Userrole
	@NotNull
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_userrole", joinColumns = { @JoinColumn(name = "user") }, inverseJoinColumns = {
			@JoinColumn(name = "userrole") })
	private List<UserRole> userroles;

	public User() {
	}

	public User(int id, String firstname, String lastname) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public User(boolean active, String addressline1, String addressline2, String addressline3, String country,
			String email, String firstname, String lastname, String mobile, String password, String state, String title,
			String username, boolean requiredChangePassword, List<UserRole> userroles, Date dob) {
		super();
		this.active = active;
		this.addressline1 = addressline1;
		this.addressline2 = addressline2;
		this.addressline3 = addressline3;
		this.country = country;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
		this.mobile = mobile;
		this.password = password;
		this.state = state;
		this.title = title;
		this.username = username;
		this.requiredChangePassword = requiredChangePassword;
		this.userroles = userroles;
		this.dob = dob;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getAddressline1() {
		return this.addressline1;
	}

	public void setAddressline1(String addressline1) {
		this.addressline1 = addressline1;
	}

	public String getAddressline2() {
		return this.addressline2;
	}

	public void setAddressline2(String addressline2) {
		this.addressline2 = addressline2;
	}

	public String getAddressline3() {
		return this.addressline3;
	}

	public void setAddressline3(String addressline3) {
		this.addressline3 = addressline3;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public boolean isRequiredChangePassword() {
		return requiredChangePassword;
	}

	public void setRequiredChangePassword(boolean requiredChangePassword) {
		this.requiredChangePassword = requiredChangePassword;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	// public Convener getConvener() {
	// return this.convener;
	// }
	//
	// public void setConvener(Convener convener) {
	// this.convener = convener;
	// }

	// public Convener addConvener(Convener convener) {
	// getConvener().add(convener);
	// convener.setUser(this);
	//
	// return convener;
	// }

	// public Convener removeConvener(Convener convener) {
	// getConvener().remove(convener);
	// convener.setUser(null);
	//
	// return convener;
	// }

	// public Student getStudent() {
	// return this.student;
	// }
	//
	// public void setStudent(Student student) {
	// this.student = student;
	// }

	// public Student addStudent(Student student) {
	// getStudent().add(student);
	// student.setUser(this);
	//
	// return student;
	// }

	// public Student removeStudent(Student student) {
	// getStudent().remove(student);
	// student.setUser(null);
	//
	// return student;
	// }

	// public Supervisor getSupervisor() {
	// return this.supervisor;
	// }
	//
	// public void setSupervisor(Supervisor supervisor) {
	// this.supervisor = supervisor;
	// }

	// public Supervisor addSupervisor(Supervisor supervisor) {
	// getSupervisor().add(supervisor);
	// supervisor.setUser(this);
	//
	// return supervisor;
	// }


	// public Supervisor removeSupervisor(Supervisor supervisor) {
	// getSupervisor().remove(supervisor);
	// supervisor.setUser(null);
	//
	// return supervisor;
	// }
//	@JsonIgnore
	public List<UserRole> getUserroles() {
		return this.userroles;
	}

//	@JsonIgnore
	public void setUserroles(List<UserRole> userroles) {
		this.userroles = userroles;
	}

}