package com.tcabs.model;

public class StudentWeeklyTask {

	private int studentId;
	
	private String studentName;
	
	private int totalHourPerWeek;
	
	private float totalCostPerWeek;

	public StudentWeeklyTask(int totalHourPerWeek, float totalCostPerWeek) {
		super();
		this.totalHourPerWeek = totalHourPerWeek;
		this.totalCostPerWeek = totalCostPerWeek;
	}

	public StudentWeeklyTask(int studentId, String studentName, int totalHourPerWeek, float totalCostPerWeek) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.totalHourPerWeek = totalHourPerWeek;
		this.totalCostPerWeek = totalCostPerWeek;
	}

	public StudentWeeklyTask() {
		super();
	}

	public int getTotalHourPerWeek() {
		return totalHourPerWeek;
	}

	public void setTotalHourPerWeek(int totalHourPerWeek) {
		this.totalHourPerWeek = totalHourPerWeek;
	}

	public float getTotalCostPerWeek() {
		return totalCostPerWeek;
	}

	public void setTotalCostPerWeek(float totalCostPerWeek) {
		this.totalCostPerWeek = totalCostPerWeek;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
}
