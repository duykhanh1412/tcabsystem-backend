package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Date;
import java.util.List;

/**
 * The persistent class for the unit database table.
 * 
 */
@Entity
@Proxy(lazy = false)
public class Unit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Size(max = 45)
	private String description;

	@NotNull
	private Date enddate;

	@NotNull
	@Size(min = 2, max = 45)
	private String name;

	@NotNull
	private Date startdate;

	@NotNull
	@Size(min = 2, max = 45)
	private String unitcode;

	// bi-directional many-to-one association to Project
	@OneToMany(mappedBy = "unit")
	@JsonIgnore
	private List<Project> projects;

	public Unit() {
	}

	public Unit(int id, String unitcode, String name) {
		super();
		this.id = id;
		this.name = name;
		this.unitcode = unitcode;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEnddate() {
		return this.enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartdate() {
		return this.startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public String getUnitcode() {
		return this.unitcode;
	}

	public void setUnitcode(String unitcode) {
		this.unitcode = unitcode;
	}

	public List<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public Project addProject(Project project) {
		getProjects().add(project);
		project.setUnit(this);

		return project;
	}

	public Project removeProject(Project project) {
		getProjects().remove(project);
		project.setUnit(null);

		return project;
	}
}