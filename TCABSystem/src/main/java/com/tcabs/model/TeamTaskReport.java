package com.tcabs.model;

public class TeamTaskReport {

	private int student;
	
	private int user;
	
	private String userName;
	
	private int week;
	
	private int totalHourPerWeek;
	
	private float totalCostPerWeek;

	public TeamTaskReport() {
		super();
	}

	public TeamTaskReport(int student, int user, String userName, int week, int totalHourPerWeek,
			float totalCostPerWeek) {
		super();
		this.student = student;
		this.user = user;
		this.userName = userName;
		this.week = week;
		this.totalHourPerWeek = totalHourPerWeek;
		this.totalCostPerWeek = totalCostPerWeek;
	}

	public int getStudent() {
		return student;
	}

	public void setStudent(int student) {
		this.student = student;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	public int getTotalHourPerWeek() {
		return totalHourPerWeek;
	}

	public void setTotalHourPerWeek(int totalHourPerWeek) {
		this.totalHourPerWeek = totalHourPerWeek;
	}

	public float getTotalCostPerWeek() {
		return totalCostPerWeek;
	}

	public void setTotalCostPerWeek(float totalCostPerWeek) {
		this.totalCostPerWeek = totalCostPerWeek;
	}
}
