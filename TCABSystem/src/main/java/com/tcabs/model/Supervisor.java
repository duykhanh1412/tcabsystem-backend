package com.tcabs.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

/**
 * The persistent class for the supervisor database table.
 * 
 */
@Entity
public class Supervisor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String officelocation;

	/**
	 *  bi-directional many-to-one association to Meeting
	 */
	@OneToMany(mappedBy = "supervisor")
	@JsonIgnore
	private List<Meeting> meetings;

	/**
	 *  bi-directional many-to-one association to User
	 */
	@OneToOne
	@JoinColumn(name = "user")
	private User user;

	/**
	 *  bi-directional many-to-one association to Team
	 */
	@OneToMany(mappedBy = "supervisor")
	@JsonIgnore
	private List<Team> teams;

	public Supervisor() {
	}

	public Supervisor(int id, int userId, String firstname, String lastname) {
		super();
		this.id = id;
		this.user = new User(userId, firstname, lastname);
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOfficelocation() {
		return this.officelocation;
	}

	public void setOfficelocation(String officelocation) {
		this.officelocation = officelocation;
	}

	public List<Meeting> getMeetings() {
		return this.meetings;
	}

	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}

	public Meeting addMeeting(Meeting meeting) {
		getMeetings().add(meeting);
		meeting.setSupervisor(this);

		return meeting;
	}

	public Meeting removeMeeting(Meeting meeting) {
		getMeetings().remove(meeting);
		meeting.setSupervisor(null);

		return meeting;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Team> getTeams() {
		return this.teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public Team addTeam(Team team) {
		getTeams().add(team);
		team.setSupervisor(this);

		return team;
	}

	public Team removeTeam(Team team) {
		getTeams().remove(team);
		team.setSupervisor(null);

		return team;
	}
}