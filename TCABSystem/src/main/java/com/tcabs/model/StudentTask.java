package com.tcabs.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * The persistent class for the student_task database table.
 * 
 */
@Entity
@Table(name = "student_task")
public class StudentTask implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private StudentTaskPK id;

	@NotNull
	private int timetaken;

	@NotNull
	private Date date;
	
	@NotNull
	private int week;
	
	// bi-directional many-to-one association to Student
	@ManyToOne
	@JoinColumn(name = "student", insertable = false, updatable = false)
	private Student student;

	// bi-directional many-to-one association to Task
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "task", insertable = false, updatable = false)
	private Task task;

	// bi-directional many-to-one association to Team
	@ManyToOne
	@JoinColumn(name = "team", insertable = false, updatable = false)
	private Team team;

	public StudentTask() {
	}

	public StudentTaskPK getId() {
		return this.id;
	}

	public void setId(StudentTaskPK id) {
		this.id = id;
	}

	public int getTimetaken() {
		return this.timetaken;
	}

	public void setTimetaken(int timetaken) {
		this.timetaken = timetaken;
	}

	@JsonBackReference("student")
	public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Task getTask() {
		return this.task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	@JsonBackReference("team")
	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

}