package com.tcabs.service;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcabs.model.Convener;
import com.tcabs.repository.ConvenerRepository;

@Service("convenerService")
public class ConvenerServiceImpl implements ConvenerService {

	@Autowired
	private ConvenerRepository convenerRepository;

	@Override
	public boolean isConvenerAlreadyInvolvedInAnUnit(int convenerId) {
		try {
			Convener convener = convenerRepository.findConvenerByUnit(convenerId);
			return convener == null ? false : true;
		} catch (NoResultException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
	}
}
