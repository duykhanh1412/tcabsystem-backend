package com.tcabs.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.tcabs.model.Project;
import com.tcabs.model.Student;
import com.tcabs.model.UploadError;
import com.tcabs.model.User;

public interface StudentService {
	public User deleteStudent(User user, HashMap<String, String> userType) throws Exception;

	public User updateStudent(User user, HashMap<String, String> userType) throws Exception;
	
	public boolean isStudentExisting(Student student) throws Exception;
	
	public boolean isStudentInvolvedInATeam(Project project, Student student) throws Exception;
	
	public boolean isStudentInvolvedInAnotherTeam(Project project, Student student, HashMap<Integer, Integer> oldStudent) throws Exception;
	
	public boolean isStudentEnrolledInTheUnit(int unitId, int studentId) throws Exception;
	
	public List<Student> findStudentsByNameAndTeamIdAndProjectId(String name, int teamId, int projectId) throws Exception;
	
	public List<Student> findStudentsByName(String name, int unitId) throws Exception;
	
	public Student findStudentByUserId(int userId) throws Exception;
	
	public void enrollUnits(MultipartFile file) throws Exception;
	
	public UploadError findUploadErrorFile(String name) throws Exception;
	
}
