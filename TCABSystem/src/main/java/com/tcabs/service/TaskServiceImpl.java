package com.tcabs.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.exception.StudentException;
import com.tcabs.exception.TaskException;
import com.tcabs.model.Student;
import com.tcabs.model.StudentTask;
import com.tcabs.model.Task;
import com.tcabs.repository.StudentTaskRepository;
import com.tcabs.repository.TaskRepository;

@Service("taskService")
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private StudentTaskRepository studentTaskRepository;
	
	@Autowired
	private StudentService studentService;

	/**
	 * The method service used to submit individual tasks. Input would be the
	 * student with a list of tasks.
	 */
	@Override
	@Transactional
	public Student submitIndividualTasks(Student student) throws Exception {
		try {
			int studentId = studentService.findStudentByUserId(student.getUser().getId()).getId();
			student.setId(studentId);
			if (isTaskAlreadySubmitted(student)) {
				throw new TaskException(null);
			}
			List<StudentTask> invalidStudentTasks = new ArrayList<>();
			for (StudentTask studentTask : student.getStudentTasks()) {
				if (studentTask.getDate().getTime() > System.currentTimeMillis()) {
					invalidStudentTasks.add(studentTask);
				}
			}
			if (!invalidStudentTasks.isEmpty()) {
				student.setStudentTasks(invalidStudentTasks);
				throw new StudentException(student);
			} else {
				for (StudentTask studentTask : student.getStudentTasks()) {
					Task task = studentTask.getTask();
					taskRepository.saveTask(task.getDeacription(), task.getRole().getId());
					int taskId = taskRepository.getMaximumTaskId();
					studentTaskRepository.saveStudentTask(studentId, taskId, studentTask.getTimetaken(),
							studentTask.getDate(), studentTask.getWeek(), studentTask.getTeam().getId());
				}
				return student;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to check whether the tasks are submitted for the
	 * provided week.
	 */
	@Override
	public boolean isTaskAlreadySubmitted(Student student) throws Exception {
		try {
			return !studentTaskRepository.findStudentTaskByStudent_IdAndTeam_IdAndWeek(student.getId(),
					student.getStudentTasks().get(0).getTeam().getId(),
					student.getStudentTasks().get(0).getWeek()).isEmpty() ? true : false;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
