package com.tcabs.service;

import java.util.List;

import com.tcabs.model.StudentTaskReport;
import com.tcabs.model.Week;

public interface StudentTaskReportService {

	public List<StudentTaskReport> findStudentTaskReportByUserId(int userId, int teamId) throws Exception;
	
	public List<StudentTaskReport> findStudentTaskReportByTeamIdAndWeek(int week, int teamId) throws Exception;

	public List<Week> findTeamTaskReportByTeamId(int teamId) throws Exception;
}
