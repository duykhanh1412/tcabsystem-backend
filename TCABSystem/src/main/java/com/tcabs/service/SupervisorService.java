package com.tcabs.service;

import java.util.List;

import com.tcabs.model.Supervisor;

public interface SupervisorService {

	public boolean isSupervisorAlreadyInvolvedInAnUnit(int supervisorId);
	
	public boolean isSupervisorExisting(int supervisorId);
	
	public boolean isSupervisorInvolvedInTeachingTheUnit(int unitId, int supervisorId) throws Exception;
	
	public List<Supervisor> findSupervisorsByName(String name, int unitId) throws Exception;
}
