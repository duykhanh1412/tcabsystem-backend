package com.tcabs.service;


import com.tcabs.model.Unit;
import java.util.List;

public interface UnitService {
	
	public Unit addUnit(Unit unit) throws Exception;
	
	public Unit updateUnit(Unit unit) throws Exception;
	
	public void deleteUnit(int unitId) throws Exception;
	
	public List<Unit> findAllUnitOfConvener(int userId) throws Exception;
	
	public List<Unit> findAllUnitOfStudent(int userId) throws Exception;
	
	public List<Unit> findUnitByName(String name) throws Exception;
	
	public Unit findUnitById(int unitId) throws Exception;
	
	public List<Unit> findUnitByUnitCode(String unitCode) throws Exception;
	
	public List<Unit> searchUnits(String name, String unitCode) throws Exception;

}
