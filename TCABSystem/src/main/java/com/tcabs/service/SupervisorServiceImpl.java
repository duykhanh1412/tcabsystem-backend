package com.tcabs.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcabs.exception.SupervisorException;
import com.tcabs.model.Supervisor;
import com.tcabs.repository.SupervisorRepository;

@Service("supervisorService")
public class SupervisorServiceImpl implements SupervisorService {

	@Autowired
	private SupervisorRepository supervisorRepository;

	/**
	 * The method used to check whether the supervisor has already involved in
	 * teaching any unit
	 */
	@Override
	public boolean isSupervisorAlreadyInvolvedInAnUnit(int supervisorId) {
		try {
			Supervisor supervisor = supervisorRepository.findSupervisorByUnit(supervisorId);
			return supervisor == null ? false : true;
		} catch (NoResultException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * The method used to check whether a supervisor is already existing
	 */
	@Override
	public boolean isSupervisorExisting(int supervisorId) {
		try {
			return supervisorRepository.findOne(supervisorId) != null ? true : false;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public List<Supervisor> findSupervisorsByName(String name, int unitId) throws Exception {
		try {
			List<Supervisor> supervisors = new ArrayList<>();
			supervisors = supervisorRepository.findSupervisorsByName("%" + name + "%", unitId);
			if(!supervisors.isEmpty()) {
				return supervisors;
			} else {
				throw new SupervisorException(null);
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public boolean isSupervisorInvolvedInTeachingTheUnit(int unitId, int supervisorId) {
		try {
			return supervisorRepository.findSupervisorByUnitIdAndSupervisorId(unitId, supervisorId) != null ? true : false;
		} catch (NoResultException ex) {
			return false;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
