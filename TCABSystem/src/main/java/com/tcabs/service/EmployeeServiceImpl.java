package com.tcabs.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.exception.AdminException;
import com.tcabs.exception.UserEmailException;
import com.tcabs.model.User;
import com.tcabs.repository.ConvenerRepository;
import com.tcabs.repository.SupervisorRepository;
import com.tcabs.repository.UserRepository;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ConvenerRepository convenerRepository;

	@Autowired
	private SupervisorRepository supervisorRepository;

	@Autowired
	private UserService userService;

	/**
	 * The service used to delete an employee. If the Employees
	 * have already involve in teaching an unit, then they are not able
	 * to be deleted
	 */
	@Override
	@Transactional
	public User deleteEmployee(User user, HashMap<String, String> userType) throws Exception {
		try {
			if (userService.isEmailAlreadyExist(user.getEmail())) {
				int userId = user.getId();
				int numberOfExistingAdmin = userRepository.getNumberExistingAdmin();
				if (userType.containsKey("ADMIN") && numberOfExistingAdmin <= 1) {
					throw new AdminException(user);
				}
				if (userType.containsKey("SUPERVISOR")) {
					supervisorRepository.deleteSupervisor(userId);
				}
				if (userType.containsKey("CONVENER")) {
					convenerRepository.deleteConvener(userId);
				}
				userRepository.deleteUser(user.getId());
				return user;

			} else {
				throw new UserEmailException(user);
			}
		} catch (DataIntegrityViolationException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * This method used to update the employee
	 */
	@Override
	@Transactional
	public User updateEmployee(User user, HashMap<String, String> userType) throws Exception {
		try {
			int numberOfExistingAdmin = userRepository.getNumberExistingAdmin();
			if (!userType.containsKey("ADMIN") && numberOfExistingAdmin <= 1) {
				throw new AdminException(user);
			}
			User oldUser = userService.findUserById(user);
			if (oldUser.getEmail().equals(user.getEmail())) {
				User updatedUser = new User();
				updatedUser = userRepository.updateUser(user, oldUser, userType);
				return updatedUser;
			} else if (!userService.isEmailAlreadyExist(user.getEmail())) {
				User updatedUser = new User();
				updatedUser = userRepository.updateUser(user, oldUser, userType);
				return updatedUser;
			} else {
				throw new UserEmailException(user);
			}
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
