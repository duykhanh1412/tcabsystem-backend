package com.tcabs.service;

import com.tcabs.model.Student;

public interface TaskService {

	public Student submitIndividualTasks(Student student) throws Exception;
	
	public boolean isTaskAlreadySubmitted(Student student) throws Exception;
}
