package com.tcabs.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcabs.exception.StudentTaskReportException;
import com.tcabs.model.StudentTaskReport;
import com.tcabs.model.StudentWeeklyTask;
import com.tcabs.model.TeamTaskReport;
import com.tcabs.model.Week;
import com.tcabs.repository.StudentTaskReportRepository;

@Service("studentTaskReportService")
public class StudentTaskReportServiceImpl implements StudentTaskReportService {

	@Autowired
	private StudentTaskReportRepository studentTaskReportRepository;

	/**
	 * The service method used to generate the report regarding to the tasks of
	 * a team member based on user ID and team ID
	 */
	@Override
	public List<StudentTaskReport> findStudentTaskReportByUserId(int userId, int teamId) throws Exception {
		try {
			List<StudentTaskReport> studentTaskReports = new ArrayList<>();
			HashMap<Integer, Integer> week = new HashMap<>();
			studentTaskReports = studentTaskReportRepository.findStudentTaskReportByUserId(userId, teamId);
			if (studentTaskReports.isEmpty()) {
				throw new StudentTaskReportException(studentTaskReports);
			} else {
				for (StudentTaskReport studentTaskReport : studentTaskReports) {
					week.put(studentTaskReport.getWeek(), studentTaskReport.getWeek());
				}
				for (StudentTaskReport studentTaskReport : studentTaskReports) {
					studentTaskReport.setNumberOfWeek(week.size());
				}
				return studentTaskReports;
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to generate the report regarding to the project
	 * information of a team
	 */
	@Override
	public List<StudentTaskReport> findStudentTaskReportByTeamIdAndWeek(int week, int teamId) throws Exception {
		try {
			List<StudentTaskReport> teamTaskReports = studentTaskReportRepository.findStudentTaskReportByWeek(week, teamId);
			if (teamTaskReports.isEmpty()) {
				throw new StudentTaskReportException(new ArrayList<>());
			} else
				return teamTaskReports;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public List<Week> findTeamTaskReportByTeamId(int teamId) throws Exception {
		try {
			List<TeamTaskReport> teamTaskReports = new ArrayList<>();
			List<Week> weeks = new ArrayList<>();
			HashMap<Integer, Week> weeksMap = new HashMap<>();
			List<Object[]> objects = studentTaskReportRepository.findTeamTaskReportByTeamId(teamId);
			for (Object[] object : objects) {
				BigDecimal totalHourPerWeek = (BigDecimal) object[4];
				Double totalCostPerWeek = (Double) object[5];
				TeamTaskReport teamTaskReport = new TeamTaskReport((int) object[0], (int) object[1],
						object[2].toString(), (int) object[3], totalHourPerWeek.intValue(),
						totalCostPerWeek.floatValue());
				teamTaskReports.add(teamTaskReport);
			}
			HashMap<Integer, String> students = new HashMap<>();
			for (TeamTaskReport teamTaskReport : teamTaskReports) {
				if (!weeksMap.containsKey(teamTaskReport.getWeek())) {
					StudentWeeklyTask studentWeeklyTask = new StudentWeeklyTask(teamTaskReport.getStudent(),
							teamTaskReport.getUserName(), teamTaskReport.getTotalHourPerWeek(),
							teamTaskReport.getTotalCostPerWeek());
					List<StudentWeeklyTask> studentWeeklyTasks = new ArrayList<>();
					studentWeeklyTasks.add(studentWeeklyTask);
					Week week = new Week(teamTaskReport.getWeek(), studentWeeklyTasks,
							teamTaskReport.getTotalHourPerWeek(), teamTaskReport.getTotalCostPerWeek());
					weeksMap.put(teamTaskReport.getWeek(), week);
					students.put(teamTaskReport.getStudent(), teamTaskReport.getUserName());
				} else {
					StudentWeeklyTask studentWeeklyTask = new StudentWeeklyTask(teamTaskReport.getStudent(),
							teamTaskReport.getUserName(), teamTaskReport.getTotalHourPerWeek(),
							teamTaskReport.getTotalCostPerWeek());
					weeksMap.get(teamTaskReport.getWeek()).getStudentWeeklyTask().add(studentWeeklyTask);
					weeksMap.get(teamTaskReport.getWeek()).setToltalHourPerWeekForTeam(
							weeksMap.get(teamTaskReport.getWeek()).getToltalHourPerWeekForTeam()
									+ teamTaskReport.getTotalHourPerWeek());
					weeksMap.get(teamTaskReport.getWeek()).setTotalCostPerWeekForTeam(
							weeksMap.get(teamTaskReport.getWeek()).getTotalCostPerWeekForTeam()
									+ teamTaskReport.getTotalCostPerWeek());
					students.put(teamTaskReport.getStudent(), teamTaskReport.getUserName());
				}
			}
			for (int i : weeksMap.keySet()) {
				if (weeksMap.get(i).getStudentWeeklyTask().size() < students.size()) {
					for (int studentId : students.keySet()) {
						boolean isStudentIdAdded = false;
						for (StudentWeeklyTask studentWeeklyTask : weeksMap.get(i).getStudentWeeklyTask()) {
							if (studentId == studentWeeklyTask.getStudentId())
								isStudentIdAdded = true;
						}
						if (!isStudentIdAdded) {
							StudentWeeklyTask studentWeeklyTask = new StudentWeeklyTask(studentId,
									students.get(studentId), 0, 0);
							weeksMap.get(i).getStudentWeeklyTask().add(studentWeeklyTask);
						}
					}
				}
				weeks.add(weeksMap.get(i));
			}
			if (weeks.isEmpty()) {
				throw new StudentTaskReportException(new ArrayList<>());
			} else
				return weeks;
		} catch (StudentTaskReportException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
