package com.tcabs.service;

import java.util.List;

import com.tcabs.model.Project;
import com.tcabs.model.Team;

public interface ProjectService {

	public Project saveProject(Project project) throws Exception;
	
	public boolean isProjectAleadyRegistered(Project project) throws Exception;
	
	public void deleteProject(int projectId) throws Exception;
	
	public List<Project> findProjectsByProjectNameAndUnitId(String projectName, int unitId) throws Exception;
	
	public Project findProjectById(int projectId) throws Exception;
	
	public Project updateProject(Project project) throws Exception;
	
	public boolean isProjectValid(Project project) throws Exception;
	
	public List<Team> allocateProject(Team[] teams) throws Exception;
	
	public List<Project> findProjectsByUnitId(int unitId) throws Exception;
	
	public void updateProjectAlocation(int teamId) throws Exception;
}
