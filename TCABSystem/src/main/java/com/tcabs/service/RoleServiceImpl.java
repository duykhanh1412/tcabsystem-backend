package com.tcabs.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcabs.exception.RoleException;
import com.tcabs.model.Role;
import com.tcabs.repository.RoleRepository;

@Service("roleServce")
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public List<Role> findProjectRolesByProjectId(int projectId) throws Exception {
		try {
			List<Role> roles = new ArrayList<>();
			roles = roleRepository.findProjectRolesByProjectId(projectId);
			if (roles.isEmpty()) {
				throw new RoleException(null);
			} else {
				return roles;
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
