package com.tcabs.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.tcabs.exception.UserEmailException;
import com.tcabs.model.User;

public interface UserService {
	
	public User addUser(User newUser, HashMap<String, String> userType) throws UserEmailException, Exception;
	
	public List<User> saveListUser(List<User> users, String userType) throws Exception;
	
	public void updateUser(User newUser);
	
	public User removeUser(User removedUser);
	
	public User getUser();
	
	public boolean isUserNameAlreadyExsist(String userName) throws Exception;
	
	public boolean isEmailAlreadyExist(String email) throws DataAccessException;
	
	public User findUserById(User user) throws Exception;
	
	public List<User> findUserByFirstNameAndEmail(String firstName, String email, int userType) throws Exception;
	
	public User findUserById(int userId) throws Exception;
}
