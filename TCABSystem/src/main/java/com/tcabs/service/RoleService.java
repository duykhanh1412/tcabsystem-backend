package com.tcabs.service;

import java.util.List;

import com.tcabs.model.Role;

public interface RoleService {

	public List<Role> findProjectRolesByProjectId(int projectId) throws Exception;
}
