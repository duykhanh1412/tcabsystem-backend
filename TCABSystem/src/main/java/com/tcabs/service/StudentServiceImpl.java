package com.tcabs.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.tcabs.exception.BulkUploadException;
import com.tcabs.exception.StudentException;
import com.tcabs.exception.UserEmailException;
import com.tcabs.exception.UserException;
import com.tcabs.model.Project;
import com.tcabs.model.Student;
import com.tcabs.model.UploadError;
import com.tcabs.model.User;
import com.tcabs.repository.StudentRepository;
import com.tcabs.repository.UploadErrorRepository;
import com.tcabs.repository.UserRepository;

@Service("studentService")
public class StudentServiceImpl implements StudentService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private UploadErrorRepository uploadErrorRepository;

	@Autowired
	private TeamService teamService;

	/**
	 * The service used to delete a student. If the students have already
	 * enrolled in an unit, they are not able to be deleted
	 */
	@Override
	@Transactional
	public User deleteStudent(User user, HashMap<String, String> userType) throws Exception {
		try {
			if (userService.isEmailAlreadyExist(user.getEmail())) {
				int userId = user.getId();
				studentRepository.deleteStudent(userId);
				userRepository.deleteUser(user.getId());
				return user;
			} else {
				throw new UserEmailException(user);
			}
		} catch (DataIntegrityViolationException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * This method used to update the user
	 */
	@Override
	@Transactional
	public User updateStudent(User user, HashMap<String, String> userType) throws Exception {
		try {
			User oldUser = userService.findUserById(user);
			if (oldUser.getEmail().equals(user.getEmail())) {
				User updatedUser = new User();
				updatedUser = userRepository.updateUser(user, oldUser, userType);
				return updatedUser;
			} else if (!userService.isEmailAlreadyExist(user.getEmail())) {
				User updatedUser = new User();
				updatedUser = userRepository.updateUser(user, oldUser, userType);
				return updatedUser;
			} else {
				throw new UserEmailException(user);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to check whether the student is existing.
	 */
	@Override
	public boolean isStudentExisting(Student student) throws Exception {
		try {
			return studentRepository.findOne(student.getId()) != null ? true : false;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to check whether a student is already involved to
	 * a team.
	 */
	@Override
	public boolean isStudentInvolvedInATeam(Project project, Student student) throws Exception {
		try {
			return teamService.findTeamByProjectIdAndStudentId(project.getId(), student.getUser().getId()) != null
					? true : false;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to check whether a student enrolled in the unit.
	 */
	@Override
	public boolean isStudentEnrolledInTheUnit(int unitId, int studentId) throws Exception {
		try {
			return studentRepository.findStudentByUnitIdAndStudentId(unitId, studentId) != null ? true : false;
		} catch (NoResultException ex) {
			return false;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to find the students based on student name and
	 * unit ID.
	 */
	@Override
	public List<Student> findStudentsByName(String name, int unitId) throws Exception {
		try {
			List<Student> students = new ArrayList<>();
			students = studentRepository.findStudentsByName("%" + name + "%", unitId);
			if (!students.isEmpty()) {
				return students;
			} else {
				throw new UserException();
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The method service used to check whether a student involve in a team.
	 */
	@Override
	public boolean isStudentInvolvedInAnotherTeam(Project project, Student student,
			HashMap<Integer, Integer> oldStudent) throws Exception {
		try {
			if (oldStudent.containsKey(student.getId())) {
				return false;
			}
			return teamService.findTeamByProjectIdAndStudentId(project.getId(), student.getId()) != null ? true : false;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to find student by student ID
	 */
	@Override
	public Student findStudentByUserId(int userId) throws Exception {
		try {
			Student student = studentRepository.findStudentByUserId(userId);
			if (student == null) {
				throw new StudentException(student);
			} else {
				return student;
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to enroll student in to units using bulk upload
	 * data
	 */
	@Override
	public void enrollUnits(MultipartFile file) throws Exception {

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		String fileType = file.getContentType();
		String fileName = file.getOriginalFilename();
		String fileNameExtension = fileName.split("\\.")[fileName.split("\\.").length - 1];
		try {
			InputStream inputStream = file.getInputStream();
			br = new BufferedReader(new InputStreamReader(inputStream));

			StringBuilder invalidRecords = new StringBuilder();
			invalidRecords.append("Student ID,Unit ID,Error Message\n");
			String firstCharacter = "\n";
			boolean failedRecords = false;

			while ((line = br.readLine()) != null) {
				// use comma as separator
				String[] enrolmentList = line.split(cvsSplitBy);
				try {
					studentRepository.saveStudentUnit(new Integer(enrolmentList[0]), new Integer(enrolmentList[1]));
				} catch (DataIntegrityViolationException ex) {
					failedRecords = true;
					invalidRecords.append(line);
					invalidRecords.append(",");
					if (ex.getCause().getCause().getMessage().indexOf("a foreign key constraint fails") > 0) {
						invalidRecords.append("The Student or Unit have not been registered.");
					} else {
						invalidRecords.append("The Student has already registered to this Unit.");
					}
					invalidRecords.append(firstCharacter);
					ex.printStackTrace();
				} catch (Exception ex) {
					failedRecords = true;
					invalidRecords.append(line);
					invalidRecords.append(",");
					invalidRecords.append("The Student or Unit input are invalid. Please enter the interger values.");
					invalidRecords.append(firstCharacter);
					ex.printStackTrace();
				}
			}

			if (failedRecords) {
				String errorFileName = "ENROLL_STUDENT_UNIT_ERRORS"
						+ new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
				uploadErrorRepository.saveEnrollStudentsIntoUnitErrors(fileType,
						errorFileName + "." + fileNameExtension, invalidRecords.toString().getBytes());
				throw new BulkUploadException(errorFileName);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * The service method used to search for the students based on student name,
	 * team ID, and project ID
	 * 
	 * @throws Exception
	 */
	@Override
	public List<Student> findStudentsByNameAndTeamIdAndProjectId(String name, int teamId, int projectId)
			throws Exception {
		try {
			List<Student> students = new ArrayList<>();
			students = studentRepository.findStudentsByNameAndTeamIdAndProjectId(name, teamId, projectId);
			if (students.isEmpty()) {
				throw new StudentException(null);
			} else
				return students;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to retrieve bulk upload error file by file name
	 * 
	 * @throws Exception
	 */
	@Override
	public UploadError findUploadErrorFile(String name) throws Exception {
		try {

			UploadError uploadError = uploadErrorRepository.findUploadErrorFile(name);
			return uploadError;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
