package com.tcabs.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.exception.ProjectException;
import com.tcabs.exception.SupervisorException;
import com.tcabs.exception.TeamException;
import com.tcabs.model.Project;
import com.tcabs.model.Team;
import com.tcabs.model.Unit;
import com.tcabs.repository.ProjectRepository;
import com.tcabs.repository.TeamRepository;

@Service("projectService")
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private SupervisorService supervisorService;

	/**
	 * This is the service method used to register new project
	 */
	@Override
	public Project saveProject(Project project) throws Exception {
		try {
			if (!isProjectAleadyRegistered(project)) {
				projectRepository.saveProject(project.getName(), project.getDescription(), project.getMaximunmembers(),
						project.getUnit().getId(), project.getDescriptionFileType(), project.getDescriptionFileName(),
						project.getStartDate(), project.getEndDate(), project.getBudget());
				return project;
			} else {
				throw new ProjectException(project);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This is the method used to check whether the project is already
	 * registered based on project name and unit code. If the project name is
	 * already existing with the same unit code, this method will return a true
	 * value.
	 */
	@Override
	public boolean isProjectAleadyRegistered(Project project) throws Exception {
		try {
			Project retrievedProject = projectRepository.findProjectByProjectNameAndUnitCode(project.getName(),
					project.getUnit().getId(), project.getStartDate(), project.getEndDate());
			return retrievedProject != null ? true : false;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This is the service method used to delete a project
	 */
	@Override
	public void deleteProject(int projectId) throws Exception {
		try {
			projectRepository.deleteProject(projectId);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * This is the service method used to find projects by project name and unit
	 * Id
	 */
	@Override
	public List<Project> findProjectsByProjectNameAndUnitId(String projectName, int unitId) throws Exception {
		try {
			List<Project> projects = new ArrayList<>();
			List<Object[]> objects = new ArrayList<>();
			objects = projectRepository.findProjectsByProjectNameAndUnitId(projectName, unitId);
			for (Object[] object : objects) {
				Unit unit = new Unit((int) object[3], object[5].toString(), object[6].toString());
				Project project = new Project((int) object[0], (int) object[2], object[1].toString(), unit, (float) object[4]);
				projects.add(project);
			}
			return projects;
		} catch (EmptyResultDataAccessException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This is the service method used to find a project by project ID
	 */
	@Override
	public Project findProjectById(int projectId) throws Exception {
		try {
			Project project = projectRepository.findProjectById(projectId);
			if (project != null) {
				return project;
			} else {
				throw new ProjectException(project);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This is the service method used to update the project. This method will
	 * check if the project description is re-uploaded. If not then the project
	 * description won't be replaced.
	 * 
	 * The project name will also be checked if the new project name is already
	 * existing in the unit. If yes, throw a project exception to tell with the
	 * controller layer that the project name is already existing
	 */
	@Override
	public Project updateProject(Project project) throws Exception {
		try {
			Project oldProject = projectRepository.findProjectById(project.getId());
			if (project.isDescriptionIncluded()) {
				if (!oldProject.getName().equals(project.getName())) {
					if (!isProjectAleadyRegistered(project)) {
						projectRepository.updateProjectWithDescription(project.getName(), project.getDescription(),
								project.getMaximunmembers(), project.getUnit().getId(),
								project.getDescriptionFileType(), project.getDescriptionFileName(), project.getId(),
								project.getBudget());
						return project;
					} else {
						throw new ProjectException(project);
					}
				} else {
					projectRepository.updateProjectWithDescription(project.getName(), project.getDescription(),
							project.getMaximunmembers(), project.getUnit().getId(), project.getDescriptionFileType(),
							project.getDescriptionFileName(), project.getId(), project.getBudget());
					return project;
				}
			} else {
				if (!oldProject.getName().equals(project.getName())) {
					if (!isProjectAleadyRegistered(project)) {
						projectRepository.updateProjectWithouthDescription(project.getName(),
								project.getMaximunmembers(), project.getUnit().getId(), project.getId(),
								project.getBudget());
						return project;
					} else {
						throw new ProjectException(project);
					}
				} else {
					projectRepository.updateProjectWithouthDescription(project.getName(), project.getMaximunmembers(),
							project.getUnit().getId(), project.getId(), project.getBudget());
					return project;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This method is used to check the size of the project description file
	 */
	@Override
	public boolean isProjectValid(Project project) throws Exception {
		return project.getDescription().length < 16777215 ? true : false;
	}

	/**
	 * This method is used to allocate the project to teams
	 */
	@Override
	@Transactional
	public List<Team> allocateProject(Team[] teams) throws Exception {
		try {
			if (!supervisorService.isSupervisorExisting(teams[0].getSupervisor().getId())) {
				throw new SupervisorException(teams[0].getSupervisor());
			}
			List<Team> allocatedTeams = new ArrayList<>();
			List<Team> invalidTeams = new ArrayList<>();
			for (int i = 0; i < teams.length; i++) {
				if (!teams[i].isTeamValid()) {
					invalidTeams.add(teams[i]);
					continue;
				} else {
					Team team = teamRepository.findTeamByTeamId(teams[i].getId());
					if (team == null) {
						invalidTeams.add(teams[i]);
					} else if (team.getTeamcount() > teams[i].getProject().getMaximunmembers()) {
						invalidTeams.add(team);
					}
				}
			}

			if (!invalidTeams.isEmpty()) {
				throw new TeamException(invalidTeams);
			} else {
				for (int i = 0; i < teams.length; i++) {
					teamRepository.allocateTeamToProject(teams[i].getId(), teams[i].getProject().getId(),
							teams[i].getSupervisor().getId());
					allocatedTeams.add(teams[i]);
				}
				return allocatedTeams;
			}
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This is the method used to find all projects that belong to an unit
	 */
	@Override
	public List<Project> findProjectsByUnitId(int unitId) throws Exception {
		try {
			Unit unit = new Unit();
			unit.setId(unitId);
			List<Project> projects = new ArrayList<>();
			List<Object[]> objects = projectRepository.findProjectsByUnitId(unitId);
			for (Object[] object : objects) {
				Project project = new Project((int) object[0], (int) object[2], object[1].toString(), unit, (float) object[3]);
				projects.add(project);
			}
			return projects;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This is the method used to update the project allocation
	 */
	@Override
	public void updateProjectAlocation(int teamId) throws Exception {
		try {
			teamRepository.updateProjectAllocation(teamId);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
	}
}
