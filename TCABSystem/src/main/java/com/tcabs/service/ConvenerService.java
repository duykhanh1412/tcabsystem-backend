package com.tcabs.service;

public interface ConvenerService {
	
	public boolean isConvenerAlreadyInvolvedInAnUnit(int convenerId);
}
