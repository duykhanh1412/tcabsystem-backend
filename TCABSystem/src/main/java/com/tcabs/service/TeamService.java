package com.tcabs.service;

import java.util.List;

import com.tcabs.model.ProjectTeamAllocation;
import com.tcabs.model.Team;

public interface TeamService {

	public List<ProjectTeamAllocation> findTeamsByProject(int projectId) throws Exception;
	
	public ProjectTeamAllocation findTeamByTeamId(int teamId) throws Exception;
	
	public List<Team> registerTeam(Team[] teams) throws Exception;
	
	public List<Team> updateTeam(Team[] teams, Team oldTeam) throws Exception;
	
	public boolean isTeamNameAlreadyRegistered(Team team) throws Exception;
	
	public Team findTeamByProjectIdAndStudentId(int projectId, int studentId) throws Exception;
	
	public List<Team> searchTeamByTeamNameAndProjectId(String teamName, int projectId) throws Exception;
	
	public void deleteTeam(int teamId) throws Exception;
	
	public Team findTeamId(int teamId) throws Exception;
}
