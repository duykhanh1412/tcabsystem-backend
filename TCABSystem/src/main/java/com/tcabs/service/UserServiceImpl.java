package com.tcabs.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.tcabs.configuration.security.SecurityUtils;
import com.tcabs.exception.UserEmailException;
import com.tcabs.model.User;
import com.tcabs.repository.UserRepository;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	/**
	 * Save the new user to the system
	 * 
	 * User name will be formed by the first character of the first name and the
	 * whole last name
	 * 
	 * Password is created using the dob with the initial format of ddMMyyyy
	 * 
	 * Set the requiredChangePassword property to be true to force the user
	 * changing the password at the first time log in to the system
	 * 
	 * Use Base64 to encrypt the password
	 * 
	 * @throws Exception
	 */
	@Override
	public User addUser(User user, HashMap<String, String> userType) throws UserEmailException, Exception {

		if (!isEmailAlreadyExist(user.getEmail())) {
			try {
				user.setUsername(automaticGeneratedUserName(user));
				user.setPassword(automaitcGeneratedPassword(user));
				user.setRequiredChangePassword(true);
				return userRepository.saveUser(user, userType);
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		} else {
			throw new UserEmailException(user);
		}
	}

	/**
	 * The method used to generate the user name based on the first character of
	 * the first name and the last character
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	private String automaticGeneratedUserName(User user) throws Exception {
		StringBuilder userName = new StringBuilder();
		userName.append(user.getFirstname().substring(0, 1));
		userName.append(user.getLastname());

		if (isUserNameAlreadyExsist(userName.toString().toLowerCase())) {
			int postfiUserName = userRepository.findUserMaxId() + 1;
			userName.append(postfiUserName);
		}
		return userName.toString().toLowerCase();
	}

	/**
	 * The method used to generate the password based on the dob
	 * 
	 * @param user
	 * @return
	 */
	private String automaitcGeneratedPassword(User user) {
		DateFormat formatter = new SimpleDateFormat("ddMMyyyy");
		String dob = formatter.format(user.getDob());
		byte[] bytesEncoded = org.springframework.security.crypto.codec.Base64.encode(dob.getBytes());
		return new String(bytesEncoded);
	}

	/**
	 * The method used to check whether the user name is already existing in the
	 * system
	 * 
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean isUserNameAlreadyExsist(String userName) throws Exception {
		User user = new User();
		try {
			user = userRepository.findByUsername(userName);
		} catch (DataAccessException e) {
			throw e;
		}
		return user != null ? true : false;
	}

	/**
	 * The method used to check whether the email is already existing in the
	 * system
	 * 
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean isEmailAlreadyExist(String email) throws DataAccessException {
		User user = new User();
		try {
			user = userRepository.findByEmail(email);
		} catch (DataAccessException ex) {
			throw ex;
		}
		return user != null ? true : false;
	}

	/**
	 * The method used to save a list of user
	 */
	@Override
	public List<User> saveListUser(List<User> users, String userType) throws Exception {
		List<User> preparedUsers = new ArrayList<>();
		for (User user : users) {
			if (!isEmailAlreadyExist(user.getEmail())) {
				user.setUsername(automaticGeneratedUserName(user));
				user.setPassword(automaitcGeneratedPassword(user));
				user.setRequiredChangePassword(true);
				preparedUsers.add(user);
			} else {
				throw new UserEmailException(user);
			}
		}
		try {
			return userRepository.saveBatchUser(preparedUsers, userType);
		} catch (DataAccessException ex) {
			throw ex;
		}
	}

	@Override
	public void updateUser(User newUser) {

	}

	@Override
	public User removeUser(User removedUser) {
		return null;
	}

	/**
	 * The method used to support the security function. This method will return
	 * the current logged in user to the front end to be processed
	 */
	@Override
	public User getUser() {
		User currentUser = new User();
		currentUser = userRepository.findByUsername(SecurityUtils.getCurrentLoginUser());
		currentUser.setPassword("");
		return currentUser;
	}

	/**
	 * This is the method used to find the user by Id
	 */
	@Override
	public User findUserById(User user) throws Exception {
		try {
			User resultUser = new User();
			resultUser = userRepository.findOne(user.getId());
			return resultUser;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This method is used to find the Users by Users' name or email
	 */
	@Override
	public List<User> findUserByFirstNameAndEmail(String name, String email, int userType) throws Exception {
		try {
			List<User> users = new ArrayList<>();
			users = userRepository.findUserByUserNameAndEmail(name, email, userType);
			return users;
		} catch (EmptyResultDataAccessException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This method is used to find the user by ID
	 */
	@Override
	public User findUserById(int userId) throws Exception {
		try {
			User user = new User();
			user = userRepository.findUserById(userId);
			return user;
		} catch (EmptyResultDataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}