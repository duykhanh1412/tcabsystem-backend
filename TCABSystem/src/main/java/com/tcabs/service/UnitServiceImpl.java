package com.tcabs.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcabs.exception.UnitException;
import com.tcabs.model.Unit;
import com.tcabs.repository.UnitRepository;

@Service("unitService")
public class UnitServiceImpl implements UnitService {

	@Autowired
	private UnitRepository unitRepository;

	/**
	 * Save the new unit to the system
	 * 
	 * @throws Exception
	 */
	@Override
	public Unit addUnit(Unit unit) throws Exception {
		try {
			return unitRepository.saveUnit(unit);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Update unit record in to the database
	 * 
	 * @throws Exception
	 */
	@Override
	public Unit updateUnit(Unit unit) throws Exception {
		try {
			return unitRepository.updateUnit(unit);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * This method will find all the current running unit that the user with
	 * userId involving in teaching which means that the current date will after
	 * the start date but before the end date.
	 */
	@Override
	@Transactional
	public List<Unit> findAllUnitOfConvener(int userId) throws Exception {
		try {
			List<Unit> units = new ArrayList<>();
			units = unitRepository.findAllCurrentUnitOfConvener(userId);
			if (!units.isEmpty()) {
				return units;
			} else {
				throw new UnitException(null);
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public void deleteUnit(int unitId) throws Exception {
		try {
			unitRepository.deleteUnit(unitId);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * The service method used to find the units based on unit name.
	 */
	@Override
	public List<Unit> findUnitByName(String name) throws Exception {
		try {
			List<Unit> units = new ArrayList<>();
			units = unitRepository.findUnitsByName("%" + name + "%");
			if (!units.isEmpty()) {
				return units;
			} else {
				throw new NoResultException();
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public Unit findUnitById(int unitId) throws Exception {
		try {
			Unit unit;
			unit = unitRepository.findUnitsById(unitId);
			if (unit != null) {
				return unit;
			} else {
				throw new NoResultException();
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public List<Unit> findUnitByUnitCode(String unitCode) throws Exception {
		try {
			List<Unit> units = new ArrayList<>();
			units = unitRepository.findUnitsByUnitCode("%" + unitCode + "%");
			if (!units.isEmpty()) {
				return units;
			} else {
				throw new NoResultException();
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * This method will find all the current running unit that the user with
	 * userId involving in learning which means that the current date will after
	 * the start date but before the end date.
	 */
	@Override
	public List<Unit> findAllUnitOfStudent(int userId) throws Exception {
		try {
			List<Unit> units = new ArrayList<>();
			units = unitRepository.findAllCurrentUnitOfStudent(userId);
			if (!units.isEmpty()) {
				return units;
			} else {
				throw new UnitException(null);
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public List<Unit> searchUnits(String name, String unitCode) throws Exception {
		try {
			List<Unit> units = new ArrayList<>();
			units = unitRepository.searchUnits("%" + name + "%","%" + unitCode + "%");
			if (!units.isEmpty()) {
				return units;
			} else {
				throw new NoResultException();
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
