package com.tcabs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.tcabs.exception.SupervisorException;
import com.tcabs.exception.TeamException;
import com.tcabs.model.ProjectTeamAllocation;
import com.tcabs.model.Student;
import com.tcabs.model.StudentInformation;
import com.tcabs.model.Team;
import com.tcabs.repository.TeamRepository;

@Service("teamService")
public class TeamServiceImpl implements TeamService {

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private SupervisorService supervisorService;

	@Autowired
	private StudentService studentService;

	/**
	 * This is the service method used to find the team by project id
	 */
	@Override
	public List<ProjectTeamAllocation> findTeamsByProject(int projectId) throws Exception {
		try {
			List<ProjectTeamAllocation> projectTeamAllocations = new ArrayList<>();
			List<Object[]> objects = teamRepository.findTeamsByProject(projectId);
			int teamId = 0;
			for (Object[] object : objects) {
				teamId = (int) object[0];
				StudentInformation studentInformation = new StudentInformation((int) object[5], object[6].toString());
				if (projectTeamAllocations.isEmpty()) {
					List<StudentInformation> students = new ArrayList<>();
					students.add(studentInformation);
					ProjectTeamAllocation projectTeamAllocation = new ProjectTeamAllocation((int) object[0],
							object[1].toString(), (int) object[2], (int) object[3], object[4].toString(), students,
							(int) object[7]);
					projectTeamAllocations.add(projectTeamAllocation);
				} else {
					ProjectTeamAllocation projectTeamAllocation = projectTeamAllocations
							.get(projectTeamAllocations.size() - 1);
					if (projectTeamAllocation.getTeamId() == teamId) {
						projectTeamAllocation.getStudents().add(studentInformation);
					} else {
						List<StudentInformation> students = new ArrayList<>();
						students.add(studentInformation);
						ProjectTeamAllocation newProjectTeamAllocation = new ProjectTeamAllocation((int) object[0],
								object[1].toString(), (int) object[2], (int) object[3], object[4].toString(), students,
								(int) object[7]);
						projectTeamAllocations.add(newProjectTeamAllocation);
					}
				}
			}
			return projectTeamAllocations;
		} catch (TeamException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to find a team by Team ID.
	 */
	@Override
	public ProjectTeamAllocation findTeamByTeamId(int teamId) throws Exception {
		try {
			Object[] object = teamRepository.findTeamById(teamId);
			ProjectTeamAllocation projectTeamAllocation = new ProjectTeamAllocation((int) object[0],
					object[5].toString(), (int) object[2], (int) object[3], "", new ArrayList<>(), (int) object[6],
					(int) object[4]);
			return projectTeamAllocation;
		} catch (TeamException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to register a team (allocate the project and
	 * supervisor to the team in the same time)
	 */
	@Override
	public List<Team> registerTeam(Team[] teams) throws Exception {
		try {
			if (!supervisorService.isSupervisorExisting(teams[0].getSupervisor().getId())
					|| !supervisorService.isSupervisorInvolvedInTeachingTheUnit(teams[0].getProject().getUnit().getId(),
							teams[0].getSupervisor().getId())) {
				throw new SupervisorException(teams[0].getSupervisor());
			}
			List<Team> allocatedTeams = new ArrayList<>();
			List<Team> invalidTeams = new ArrayList<>();
			for (int i = 0; i < teams.length; i++) {
				List<Student> invalidStudents = new ArrayList<>();
				HashMap<Integer, Integer> invalidStudentsMap = new HashMap<>();
				if (teams[i].getName().equals("")) {
					teams[i].setStudents(invalidStudents);
					invalidTeams.add(teams[i]);
					continue;
				} else {
					if (isTeamNameAlreadyRegistered(teams[i])) {
						teams[i].setStudents(invalidStudents);
						invalidTeams.add(teams[i]);
					} else if (teams[i].getStudents().size() > teams[i].getProject().getMaximunmembers()) {
						teams[i].setStudents(invalidStudents);
						invalidTeams.add(teams[i]);
					} else {
						HashMap<Integer, Integer> studentIds = new HashMap<>();
						for (Student student : teams[i].getStudents()) {
							if (studentIds.containsKey(student.getId())) {
								if (!invalidStudentsMap.containsKey(student.getId())) {
									invalidStudents.add(student);
									invalidStudentsMap.put(student.getId(), student.getId());
									continue;
								} else
									continue;
							} else {
								studentIds.put(student.getId(), student.getId());
							}
							
							if (!studentService.isStudentExisting(student)
									|| !studentService.isStudentEnrolledInTheUnit(
											teams[i].getProject().getUnit().getId(), student.getId())
									|| studentService.isStudentInvolvedInATeam(teams[i].getProject(), student)) {
								if (!invalidStudentsMap.containsKey(student.getId())) {
									invalidStudents.add(student);
									invalidStudentsMap.put(student.getId(), student.getId());
								}
							}
						}
						if (!invalidStudents.isEmpty()) {
							teams[i].setStudents(invalidStudents);
							invalidTeams.add(teams[i]);
						}
					}
				}
			}

			if (!invalidTeams.isEmpty()) {
				throw new TeamException(invalidTeams);
			} else {
				for (int i = 0; i < teams.length; i++) {
					teamRepository.save(teams[i]);
					allocatedTeams.add(teams[i]);
				}
				return allocatedTeams;
			}
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to check whether a team is already registered.
	 */
	@Override
	public boolean isTeamNameAlreadyRegistered(Team team) throws Exception {
		try {
			return teamRepository.findTeamByTeamNameAndProjectId(team.getName(), team.getProject().getId()) != null
					? true : false;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to find a team by Project ID and Student ID.
	 */
	@Override
	public Team findTeamByProjectIdAndStudentId(int projectId, int userId) throws Exception {
		try {
			Team team = new Team();
			team = teamRepository.findTeamByProjectIdAndUserId(userId, projectId);
			return team;
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to search for teams using team name and Project
	 * ID.
	 */
	@Override
	public List<Team> searchTeamByTeamNameAndProjectId(String teamName, int projectId) throws Exception {
		try {
			List<Team> teams = new ArrayList<>();
			teams = teamRepository.searchTeamByTeamNameAndProjectId("%" + teamName + "%", projectId);
			if (!teams.isEmpty()) {
				return teams;
			} else {
				throw new TeamException(teams);
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to delete a team.
	 */
	@Override
	public void deleteTeam(int teamId) throws Exception {
		try {
			teamRepository.deleteTeam(teamId);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to find a team by Team ID.
	 */
	@Override
	public Team findTeamId(int teamId) throws Exception {
		List<Team> teams = new ArrayList<>();
		try {
			Team result = new Team();
			teams = teamRepository.searchTeamByTeamId(teamId);
			if (teams.isEmpty()) {
				return null;
			} else {
				boolean isFirstRecord = true;
				for (Team team : teams) {
					if (isFirstRecord) {
						result = team;
						result.setStudents(new ArrayList<>());
						result.getStudents().add(team.getStudent());
						isFirstRecord = false;
					} else {
						result.getStudents().add(team.getStudent());
					}
				}
				return result;
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	/**
	 * The service method used to update a team formation.
	 */
	@Override
	public List<Team> updateTeam(Team[] teams, Team oldTeam) throws Exception {
		try {
			if (!supervisorService.isSupervisorExisting(teams[0].getSupervisor().getId())
					|| !supervisorService.isSupervisorInvolvedInTeachingTheUnit(teams[0].getProject().getUnit().getId(),
							teams[0].getSupervisor().getId())) {
				throw new SupervisorException(teams[0].getSupervisor());
			}
			List<Team> allocatedTeams = new ArrayList<>();
			List<Team> invalidTeams = new ArrayList<>();
			for (int i = 0; i < teams.length; i++) {
				List<Student> invalidStudents = new ArrayList<>();
				HashMap<Integer, Integer> invalidStudentsMap = new HashMap<>();
				if (teams[i].getName().equals("")) {
					teams[i].setStudents(invalidStudents);
					invalidTeams.add(teams[i]);
					continue;
				} else {
					if (!teams[i].getName().equals(oldTeam.getName())) {
						if (isTeamNameAlreadyRegistered(teams[i])) {
							teams[i].setStudents(invalidStudents);
							invalidTeams.add(teams[i]);
						}
					} else if (teams[i].getStudents().size() > teams[i].getProject().getMaximunmembers()) {
						teams[i].setStudents(invalidStudents);
						invalidTeams.add(teams[i]);
					} else {
						HashMap<Integer, Integer> studentIds = new HashMap<>();
						HashMap<Integer, Integer> oldStudents = new HashMap<>();
						for (Student student : oldTeam.getStudents()) {
							oldStudents.put(student.getId(), student.getId());
						}
						for (Student student : teams[i].getStudents()) {
							if (studentIds.containsKey(student.getId())) {
								if (!invalidStudentsMap.containsKey(student.getId())) {
									invalidStudents.add(student);
									invalidStudentsMap.put(student.getId(), student.getId());
									continue;
								} else
									continue;
							} else {
								studentIds.put(student.getId(), student.getId());
							}
							if (!studentService.isStudentExisting(student)
									|| !studentService.isStudentEnrolledInTheUnit(
											teams[i].getProject().getUnit().getId(), student.getId())
									|| studentService.isStudentInvolvedInAnotherTeam(teams[i].getProject(), student,
											oldStudents)) {
								if (!invalidStudentsMap.containsKey(student.getId())) {
									invalidStudents.add(student);
									invalidStudentsMap.put(student.getId(), student.getId());
								}
							}
						}
						if (!invalidStudents.isEmpty()) {
							teams[i].setStudents(invalidStudents);
							invalidTeams.add(teams[i]);
						}
					}
				}
			}

			if (!invalidTeams.isEmpty()) {
				throw new TeamException(invalidTeams);
			} else {
				for (int i = 0; i < teams.length; i++) {
					teamRepository.save(teams[i]);
					allocatedTeams.add(teams[i]);
				}
				return allocatedTeams;
			}
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
}
