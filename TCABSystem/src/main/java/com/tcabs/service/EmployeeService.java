package com.tcabs.service;

import java.util.HashMap;

import com.tcabs.model.User;

public interface EmployeeService {
	
	public User deleteEmployee(User user, HashMap<String, String> userType) throws Exception;
	
	public User updateEmployee(User user, HashMap<String, String> userType) throws Exception;
}
