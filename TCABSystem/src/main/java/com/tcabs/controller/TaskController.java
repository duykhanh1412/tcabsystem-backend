package com.tcabs.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcabs.exception.StudentException;
import com.tcabs.exception.TaskException;
import com.tcabs.model.Student;
import com.tcabs.service.TaskService;

@RestController
public class TaskController {

	@Autowired
	private TaskService taskService;

	@RequestMapping(value = "/task", method = RequestMethod.POST)
	public ResponseEntity<Student> submitIndividualTasks(@Valid @RequestBody Student student) {
		try {
			Student result = taskService.submitIndividualTasks(student);
			return new ResponseEntity<Student>(result, HttpStatus.OK);
		} catch (TaskException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Student>(student, HttpStatus.FOUND);
		} catch (StudentException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Student>(ex.getStudent(), HttpStatus.BAD_REQUEST);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Student>(student, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
