package com.tcabs.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tcabs.model.User;
import com.tcabs.service.UserService;

@RestController
public class SecurityController {

	private final Logger log = LoggerFactory.getLogger(SecurityController.class);

	@Autowired
	private UserService userService;

	/**
	 * Controller method to return the current logged in user
	 * 
	 * @return
	 */
	@RequestMapping(value = "/security/account", method = GET)
	public ResponseEntity<User> getUserName() {
		User currentUser = new User();
		try {
			currentUser = userService.getUser();
			return new ResponseEntity<User>(currentUser, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage());
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
	}
}