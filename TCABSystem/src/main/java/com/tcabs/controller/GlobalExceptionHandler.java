package com.tcabs.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.tcabs.configuration.security.SecurityUtils;
import com.tcabs.exception.TcabException;

@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * Resolve the Exception with exceed maximum file size
	 * 
	 * @param e
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ExceptionHandler(MultipartException.class)
	public void handle(MultipartException ex, HttpServletResponse response) throws IOException{
//		TcabException tcabException = new TcabException("The File Size is too large", ex.getMessage());
		SecurityUtils.sendError(response, ex, HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE, "The Upload File is Too Large");
//		return new ResponseEntity<TcabException>(tcabException, HttpStatus.PAYLOAD_TOO_LARGE);
	}
}
