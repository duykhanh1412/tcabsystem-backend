package com.tcabs.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tcabs.exception.ProjectException;
import com.tcabs.exception.SupervisorException;
import com.tcabs.exception.TeamException;
import com.tcabs.model.Project;
import com.tcabs.model.ProjectTeamAllocation;
import com.tcabs.model.Team;
import com.tcabs.model.Unit;
import com.tcabs.service.ProjectService;
import com.tcabs.service.TeamService;

@RestController
public class ProjectController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private TeamService teamService;

	/**
	 * This is the controller method used to control the flow of the Project
	 * Registration process
	 * 
	 * @param fileDescription
	 * @param name
	 * @param maximunmembers
	 * @param unitId
	 * @return
	 */
	@RequestMapping(value = "/project", method = RequestMethod.POST)
	public ResponseEntity<Project> registerProject(@RequestParam("description") MultipartFile fileDescription,
			@RequestParam("projectName") String projectName, @RequestParam("maximunmembers") int maximunmembers,
			@RequestParam("unit") int unitId, @RequestParam("startdate") Date startdate,
			@RequestParam("enddate") Date enddate, @RequestParam("budget") float budget) {
		Unit unit = new Unit();
		unit.setId(unitId);
		String descriptionFileType = fileDescription.getContentType();
		String descriptionFileName = fileDescription.getOriginalFilename();
		try {
			byte[] description = fileDescription.getBytes();
			Project savedProject = new Project(description, maximunmembers, projectName, unit, descriptionFileType,
					descriptionFileName, startdate, enddate, budget);
			projectService.saveProject(savedProject);
			return new ResponseEntity<Project>(savedProject, HttpStatus.CREATED);
		} catch (JpaSystemException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(new Project(null, maximunmembers, projectName, unit, descriptionFileType,
					descriptionFileName, startdate, enddate, budget), HttpStatus.PAYLOAD_TOO_LARGE);
		} catch (DataException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(new Project(null, maximunmembers, projectName, unit, descriptionFileType,
					descriptionFileName, startdate, enddate, budget), HttpStatus.NOT_ACCEPTABLE);
		} catch (ProjectException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(new Project(null, maximunmembers, projectName, unit, descriptionFileType,
					descriptionFileName, startdate, enddate, budget), HttpStatus.FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(new Project(null, maximunmembers, projectName, unit, descriptionFileType,
					descriptionFileName, startdate, enddate, budget), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * This is the controller method used to control the flow of the Project
	 * Update process
	 * 
	 * @param fileDescription
	 * @param name
	 * @param maximunmembers
	 * @param unitId
	 * @return
	 */
	@RequestMapping(value = "/project", method = RequestMethod.PUT)
	public ResponseEntity<Project> updateProject(
			@RequestParam(value = "description", required = false) MultipartFile fileDescription,
			@RequestParam("projectName") String projectName, @RequestParam("maximunmembers") int maximunmembers,
			@RequestParam("unit") int unitId, @RequestParam("projectId") int projectId,
			@RequestParam("startdate") Date startdate, @RequestParam("enddate") Date enddate,
			@RequestParam("budget") float budget) {
		Unit unit = new Unit();
		unit.setId(unitId);
		String descriptionFileType = "";
		String descriptionFileName = "";
		byte[] description = null;
		try {
			if (fileDescription != null) {
				descriptionFileType = fileDescription.getContentType();
				descriptionFileName = fileDescription.getOriginalFilename();
				description = fileDescription.getBytes();
			}
			Project updatedProject = new Project(description, maximunmembers, projectName, unit, descriptionFileType,
					descriptionFileName, startdate, enddate, budget);
			updatedProject.setId(projectId);
			projectService.updateProject(updatedProject);
			return new ResponseEntity<Project>(updatedProject, HttpStatus.OK);
		} catch (DataException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(new Project(null, maximunmembers, projectName, unit, descriptionFileType,
					descriptionFileName, startdate, enddate, budget), HttpStatus.NOT_ACCEPTABLE);
		} catch (ProjectException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(new Project(null, maximunmembers, projectName, unit, descriptionFileType,
					descriptionFileName, startdate, enddate, budget), HttpStatus.FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(new Project(null, maximunmembers, projectName, unit, descriptionFileType,
					descriptionFileName, startdate, enddate, budget), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * This is the controller method used to control the flow of the Project
	 * Deletion process
	 * 
	 * @param project
	 * @return
	 */
	@RequestMapping(value = "/project/{projectId}", method = RequestMethod.DELETE)
	public ResponseEntity<Project> deleteProject(@PathVariable int projectId) {
		Project deletedProject = new Project();
		deletedProject.setId(projectId);
		try {
			projectService.deleteProject(projectId);
			return new ResponseEntity<Project>(deletedProject, HttpStatus.OK);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(deletedProject, HttpStatus.BAD_REQUEST);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(deletedProject, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to search projects based on project name and
	 * unit id
	 * 
	 * @param firstName
	 * @param email
	 * @return
	 */
	@RequestMapping(value = "/project", method = RequestMethod.GET)
	public ResponseEntity<List<Project>> searchProjects(@RequestParam String projectName, @RequestParam int unitId) {
		List<Project> projects = new ArrayList<>();
		try {
			projects = projectService.findProjectsByProjectNameAndUnitId(projectName, unitId);
			return new ResponseEntity<List<Project>>(projects, HttpStatus.OK);
		} catch (EmptyResultDataAccessException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Project>>(projects, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Project>>(projects, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to search project by project ID
	 * 
	 * @param projectName
	 * @param unitId
	 * @return
	 */
	@RequestMapping(value = "/project/{projectId}", method = RequestMethod.GET)
	public ResponseEntity<Project> searchProject(@PathVariable int projectId) {
		Project project = new Project();
		try {
			project = projectService.findProjectById(projectId);
			return new ResponseEntity<Project>(project, HttpStatus.OK);
		} catch (EmptyResultDataAccessException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(project, HttpStatus.NOT_FOUND);
		} catch (ProjectException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(project, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Project>(project, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to download the project description
	 * 
	 * @param projectId
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/project/description/{projectId}", method = RequestMethod.GET)
	public void downloadDocument(@PathVariable int projectId, HttpServletResponse response) throws IOException {
		Project project;
		try {
			project = projectService.findProjectById(projectId);
			response.setContentType(project.getDescriptionFileType());
			response.setContentLength(project.getDescription().length);
			response.setHeader("Content-Disposition",
					"attachment; filename=\"" + project.getDescriptionFileName() + "\"");
			FileCopyUtils.copy(project.getDescription(), response.getOutputStream());
		} catch (ProjectException ex) {
			response.setStatus(404);
			ex.printStackTrace();
		} catch (Exception ex) {
			response.setStatus(500);
			ex.printStackTrace();
		}
	}

	/**
	 * The controller method used to control the flow of the project allocation
	 * process
	 * 
	 * @param teams
	 * @return
	 */
	@RequestMapping(value = "/project/allocation", method = RequestMethod.POST)
	public ResponseEntity<List<Team>> allocateProject(@RequestBody Team[] teams) {
		List<Team> allocatedTeam = new ArrayList<>();
		try {
			allocatedTeam = projectService.allocateProject(teams);
			return new ResponseEntity<List<Team>>(allocatedTeam, HttpStatus.CREATED);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(allocatedTeam, HttpStatus.NOT_ACCEPTABLE);
		} catch (TeamException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(ex.getInvalidTeams(), HttpStatus.BAD_REQUEST);
		} catch (SupervisorException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(allocatedTeam, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(allocatedTeam, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * This is the controller method used to control the process of updating
	 * project allocation
	 * 
	 * @param projectId
	 * @return
	 */
	@RequestMapping(value = "/project/allocation/{teamId}", method = RequestMethod.DELETE)
	public ResponseEntity<Team> updateProjectAllocation(@PathVariable int teamId) {
		Team updatedTeam = new Team();
		updatedTeam.setId(teamId);
		try {
			projectService.updateProjectAlocation(teamId);
			return new ResponseEntity<Team>(updatedTeam, HttpStatus.OK);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Team>(updatedTeam, HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Team>(updatedTeam, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to control the process of getting all the
	 * project allocation based on the Project ID
	 * 
	 * @param projectId
	 * @return
	 */
	@RequestMapping(value = "/project/allocation/{projectId}", method = RequestMethod.GET)
	public ResponseEntity<List<ProjectTeamAllocation>> getProjectAllocation(@PathVariable int projectId) {
		List<ProjectTeamAllocation> projectTeamAllocations = new ArrayList<>();
		try {
			projectTeamAllocations = teamService.findTeamsByProject(projectId);
			return new ResponseEntity<List<ProjectTeamAllocation>>(projectTeamAllocations, HttpStatus.OK);
		} catch (TeamException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<ProjectTeamAllocation>>(projectTeamAllocations, HttpStatus.NOT_FOUND);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<ProjectTeamAllocation>>(projectTeamAllocations, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<ProjectTeamAllocation>>(projectTeamAllocations,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to control the flow of the projects search
	 * based on unit process
	 * 
	 * @param unitId
	 * @return
	 */
	@RequestMapping(value = "/projects/{unitId}", method = RequestMethod.GET)
	public ResponseEntity<List<Project>> findProjects(@PathVariable int unitId) {
		List<Project> projects = new ArrayList<>();
		try {
			projects = projectService.findProjectsByUnitId(unitId);
			return new ResponseEntity<List<Project>>(projects, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Project>>(projects, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Project>>(projects, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
