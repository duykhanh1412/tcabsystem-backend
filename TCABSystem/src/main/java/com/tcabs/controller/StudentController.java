package com.tcabs.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.tcabs.exception.BulkUploadException;
import com.tcabs.exception.ProjectException;
import com.tcabs.exception.StudentException;
import com.tcabs.exception.UserEmailException;
import com.tcabs.exception.UserException;
import com.tcabs.model.Response;
import com.tcabs.model.Student;
import com.tcabs.model.UploadError;
import com.tcabs.model.User;
import com.tcabs.model.UserRole;
import com.tcabs.service.StudentService;
import com.tcabs.service.UserService;

@RestController
public class StudentController {

	@Autowired
	private UserService userService;

	@Autowired
	private StudentService studentService;

	/**
	 * Controller method to register new user
	 * 
	 * @param newUser
	 * @return
	 */
	@RequestMapping(value = "/student", method = POST)
	public ResponseEntity<User> registerUser(@RequestBody User user) {
		User addedUser = new User();
		HashMap<String, String> userType = new HashMap<>();
		userType.put("STUDENT", "STUDENT");
		try {
			addedUser = userService.addUser(user, userType);
		} catch (UserEmailException ex) {
			return new ResponseEntity<User>(ex.getUser(), HttpStatus.FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<User>(addedUser, HttpStatus.CREATED);
	}

	/**
	 * The controller method to delete a student
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/student", method = RequestMethod.PUT)
	public ResponseEntity<User> deleteStudent(@RequestBody User user) {
		User deletedStudent = new User();
		HashMap<String, String> employeeType = new HashMap<>();
		for (UserRole userRole : user.getUserroles()) {
			employeeType.put(userRole.getName(), userRole.getName());
		}
		try {
			deletedStudent = studentService.deleteStudent(user, employeeType);
			return new ResponseEntity<User>(deletedStudent, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.BAD_REQUEST);
		} catch (UserEmailException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.PRECONDITION_REQUIRED);
		} catch (UserException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.PRECONDITION_FAILED);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method to update a student
	 */
	@RequestMapping(value = "/student/{studentId}", method = RequestMethod.PUT)
	public ResponseEntity<User> updateStudent(@Valid @RequestBody User user) {
		User updatedStudent = new User();
		HashMap<String, String> employeeType = new HashMap<>();
		for (UserRole userRole : user.getUserroles()) {
			employeeType.put(userRole.getName(), userRole.getName());
		}
		try {
			updatedStudent = studentService.updateStudent(user, employeeType);
			return new ResponseEntity<User>(updatedStudent, HttpStatus.CREATED);
		} catch (UserEmailException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(ex.getUser(), HttpStatus.FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to search students based on student first name
	 * and email
	 * 
	 * @param firstName
	 * @param email
	 * @return
	 */
	@RequestMapping(value = "/student", method = RequestMethod.GET)
	public ResponseEntity<List<User>> searchStudent(@RequestParam String name, @RequestParam String email) {
		List<User> users = new ArrayList<>();
		try {
			users = userService.findUserByFirstNameAndEmail(name, email, 4);
			return new ResponseEntity<List<User>>(users, HttpStatus.OK);
		} catch (EmptyResultDataAccessException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<User>>(users, HttpStatus.NOT_FOUND);
		} catch (UserException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<User>>(users, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<User>>(users, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to get an student by his/her id
	 * 
	 * @param studentId
	 * @return
	 */
	@RequestMapping(value = "/student/{studentId}", method = RequestMethod.GET)
	public ResponseEntity<User> searchStudent(@PathVariable int studentId) {
		User user = new User();
		try {
			user = userService.findUserById(studentId);
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
		} catch (UserException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method to search for the student based on student name and
	 * unit ID
	 * 
	 * @param name
	 * @param unitId
	 * @return
	 */
	@RequestMapping(value = "/students", method = RequestMethod.GET)
	public ResponseEntity<List<Student>> searchStudents(@RequestParam String name, @RequestParam int unitId) {
		List<Student> students = new ArrayList<>();
		try {
			if (unitId == 0) {
				return new ResponseEntity<List<Student>>(students, HttpStatus.NOT_ACCEPTABLE);
			}
			students = studentService.findStudentsByName(name, unitId);
			return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Student>>(students, HttpStatus.NOT_FOUND);
		} catch (UserException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Student>>(students, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Student>>(students, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method to search for students based on student team ID and
	 * project ID
	 */
	@RequestMapping(value = "/students/{teamId}/{projectId}", method = RequestMethod.GET)
	public ResponseEntity<List<Student>> searchStudentsByNameAndTeamIdAndProjectId(@RequestParam String name,
			@PathVariable int teamId, @PathVariable int projectId) {
		List<Student> students = new ArrayList<>();
		try {
			if (teamId == 0 || projectId == 0) {
				return new ResponseEntity<List<Student>>(students, HttpStatus.NOT_ACCEPTABLE);
			}
			students = studentService.findStudentsByNameAndTeamIdAndProjectId(name, teamId, projectId);
			return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Student>>(students, HttpStatus.NOT_FOUND);
		} catch (StudentException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Student>>(students, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Student>>(students, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/***
	 * The controller method to enroll students in to units using bulk upload
	 * functionality
	 * 
	 * @param file
	 * @return response
	 */
	@RequestMapping(value = "/student/enroll", method = RequestMethod.POST)
	public ResponseEntity<Response> handleFileUpload(@RequestParam("file") MultipartFile file) {
		try {
			studentService.enrollUnits(file);
			Response response = new Response("", "Enroll Units bulk upload is successfull");
			return new ResponseEntity<Response>(response, HttpStatus.OK);
		} catch (BulkUploadException e) {
			e.printStackTrace();
			Response response = new Response("", e.getErrorFile());
			return new ResponseEntity<Response>(response, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			Response response = new Response("", "Enroll Units failed");
			return new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to download the student unit enrollment bulk
	 * upload errors
	 * 
	 * @param errorFile
	 * @param response
	 */
	@RequestMapping(value = "/student/enroll/{errorFile}", method = RequestMethod.GET)
	public void handlefileUploadError(@PathVariable String errorFile, HttpServletResponse response) throws IOException {
		UploadError error;
		try {
			error = studentService.findUploadErrorFile(errorFile);
			response.setContentType(error.getType());
			response.setContentLength(error.getData().length);
			response.setHeader("Content-Disposition", "attachment; filename=\"" + errorFile + ".csv\"");
			FileCopyUtils.copy(error.getData(), response.getOutputStream());
		} catch (ProjectException ex) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
			ex.printStackTrace();
		} catch (Exception ex) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			ex.printStackTrace();
		}
	}

}