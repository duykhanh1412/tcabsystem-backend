package com.tcabs.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tcabs.exception.SupervisorException;
import com.tcabs.model.Supervisor;
import com.tcabs.service.SupervisorService;

@RestController
public class SupervisorController {

	@Autowired
	private SupervisorService supervisorService;
	
	@RequestMapping(value = "/supervisor", method = RequestMethod.GET)
	public ResponseEntity<List<Supervisor>> searchSupervisors(@RequestParam String name, @RequestParam int unitId) {
		List<Supervisor> supervisors = new ArrayList<>();
		try {
			if(unitId == 0) {
				return new ResponseEntity<List<Supervisor>>(supervisors, HttpStatus.NOT_ACCEPTABLE);
			}
			supervisors = supervisorService.findSupervisorsByName(name, unitId);
			return new ResponseEntity<List<Supervisor>>(supervisors, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Supervisor>>(supervisors, HttpStatus.NOT_FOUND);
		} catch (SupervisorException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Supervisor>>(supervisors, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Supervisor>>(supervisors, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}