package com.tcabs.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tcabs.exception.AdminException;
import com.tcabs.exception.UserEmailException;
import com.tcabs.exception.UserException;
import com.tcabs.model.User;
import com.tcabs.model.UserRole;
import com.tcabs.service.EmployeeService;
import com.tcabs.service.UserService;

@RestController
public class EmployeeController {

	@Autowired
	private UserService userService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * The controller method to register a new employee
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/employee", method = POST)
	public ResponseEntity<User> registerEmployee(@Valid @RequestBody User user) {
		User addedEmployee = new User();
		HashMap<String, String> employeeType = new HashMap<>();
		for (UserRole userRole : user.getUserroles()) {
			employeeType.put(userRole.getName(), userRole.getName());
		}
		try {
			addedEmployee = userService.addUser(user, employeeType);
			return new ResponseEntity<User>(addedEmployee, HttpStatus.CREATED);
		} catch (UserEmailException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(ex.getUser(), HttpStatus.FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method to delete an employee
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/employee", method = RequestMethod.PUT)
	public ResponseEntity<User> deleteEmployee(@RequestBody User user) {
		User deletedEmployee = new User();
		HashMap<String, String> employeeType = new HashMap<>();
		for (UserRole userRole : user.getUserroles()) {
			employeeType.put(userRole.getName(), userRole.getName());
		}
		try {
			deletedEmployee = employeeService.deleteEmployee(user, employeeType);
			return new ResponseEntity<User>(deletedEmployee, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.BAD_REQUEST);
		} catch (AdminException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(ex.getUser(), HttpStatus.NOT_ACCEPTABLE);
		} catch (UserEmailException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.PRECONDITION_REQUIRED);
		} catch (UserException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.PRECONDITION_FAILED);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to update an employee
	 */
	@RequestMapping(value = "/employee/{employeeId}", method = RequestMethod.PUT)
	public ResponseEntity<User> updateEmployee(@Valid @RequestBody User user) {
		User updatedEmployee = new User();
		HashMap<String, String> employeeType = new HashMap<>();
		for (UserRole userRole : user.getUserroles()) {
			employeeType.put(userRole.getName(), userRole.getName());
		}
		try {
			updatedEmployee = employeeService.updateEmployee(user, employeeType);
			return new ResponseEntity<User>(updatedEmployee, HttpStatus.CREATED);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.BAD_REQUEST);
		} catch (AdminException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(ex.getUser(), HttpStatus.NOT_ACCEPTABLE);
		} catch (UserEmailException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(ex.getUser(), HttpStatus.FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to search employees based on employee first
	 * name and email
	 * 
	 * @param firstName
	 * @param email
	 * @return
	 */
	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	public ResponseEntity<List<User>> searchEmployee(@RequestParam String name, @RequestParam String email) {
		List<User> users = new ArrayList<>();
		try {
			users = userService.findUserByFirstNameAndEmail(name, email, 1);
			return new ResponseEntity<List<User>>(users, HttpStatus.OK);
		} catch (EmptyResultDataAccessException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<User>>(users, HttpStatus.NOT_FOUND);
		} catch (UserException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<User>>(users, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<User>>(users, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to get an employee by his/her id
	 * 
	 * @param employeeId
	 * @return
	 */
	@RequestMapping(value = "/employee/{employeeId}", method = RequestMethod.GET)
	public ResponseEntity<User> searchEmployee(@PathVariable int employeeId) {
		User user = new User();
		try {
			user = userService.findUserById(employeeId);
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
		} catch (UserException ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<User>(user, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}