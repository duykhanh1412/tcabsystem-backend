package com.tcabs.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tcabs.exception.StudentTaskReportException;
import com.tcabs.model.StudentTaskReport;
import com.tcabs.model.Week;
import com.tcabs.service.StudentTaskReportService;

@RestController
public class ReportController {

	@Autowired
	private StudentTaskReportService studentTaskReportService;

	/**
	 * The controller method used to generate the report regarding to the tasks
	 * of a student based on user ID and team ID
	 * 
	 * @param userId
	 * @param teamId
	 * @return
	 */
	@RequestMapping(value = "/report/studentTask/{teamId}/{userId}", method = RequestMethod.GET)
	public ResponseEntity<List<StudentTaskReport>> findStudentTaskReportByUserId(@PathVariable int userId,
			@PathVariable int teamId) {
		List<StudentTaskReport> studentTaskReports = new ArrayList<>();
		try {
			studentTaskReports = studentTaskReportService.findStudentTaskReportByUserId(userId, teamId);
			return new ResponseEntity<List<StudentTaskReport>>(studentTaskReports, HttpStatus.OK);
		} catch (StudentTaskReportException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<StudentTaskReport>>(ex.getStudentTaskReports(), HttpStatus.NOT_FOUND);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<StudentTaskReport>>(studentTaskReports, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<StudentTaskReport>>(studentTaskReports, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to generate the report regarding to the tasks
	 * of a student based on user ID and team ID
	 * 
	 * @param userId
	 * @param teamId
	 * @return
	 */
	@RequestMapping(value = "/report/weeklyStudentTask/{teamId}", method = RequestMethod.GET)
	public ResponseEntity<List<StudentTaskReport>> findStudentTaskReportByTeamIdAndWeek(@RequestParam int week,
			@PathVariable int teamId) {
		List<StudentTaskReport> studentTaskReports = new ArrayList<>();
		try {
			studentTaskReports = studentTaskReportService.findStudentTaskReportByTeamIdAndWeek(week, teamId);
			return new ResponseEntity<List<StudentTaskReport>>(studentTaskReports, HttpStatus.OK);
		} catch (StudentTaskReportException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<StudentTaskReport>>(ex.getStudentTaskReports(), HttpStatus.NOT_FOUND);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<StudentTaskReport>>(studentTaskReports, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<StudentTaskReport>>(studentTaskReports, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * The controller method used to generate the report regarding to the
	 * project information of a team
	 * 
	 * @param userId
	 * @param teamId
	 * @return
	 */
	@RequestMapping(value = "/report/teamTask/{teamId}", method = RequestMethod.GET)
	public ResponseEntity<List<Week>> findTeamTaskReportByTeamId(@PathVariable int teamId) {
		List<Week> teamTaskReports = new ArrayList<>();
		try {
			teamTaskReports = studentTaskReportService.findTeamTaskReportByTeamId(teamId);
			return new ResponseEntity<List<Week>>(teamTaskReports, HttpStatus.OK);
		} catch (StudentTaskReportException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Week>>(teamTaskReports, HttpStatus.NOT_FOUND);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Week>>(teamTaskReports, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Week>>(teamTaskReports, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
