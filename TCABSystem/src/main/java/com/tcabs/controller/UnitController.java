package com.tcabs.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tcabs.exception.UnitException;
import com.tcabs.model.Unit;
import com.tcabs.service.UnitService;

@RestController
public class UnitController {

	@Autowired
	private UnitService unitService;

	/**
	 * The controller method to register unit
	 * 
	 * @param unit
	 * @return
	 */
	@RequestMapping(value = "/unit", method = POST)
	public ResponseEntity<Unit> registerUnit(@Valid @RequestBody Unit unit) {
		try {
			Unit addedUnit = new Unit();
			addedUnit = unitService.addUnit(unit);
			return new ResponseEntity<Unit>(addedUnit, HttpStatus.CREATED);
			// Unitcode exist exception
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Unit>(unit, HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Unit>(unit, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method to update unit record
	 * 
	 * @param unit
	 * @return
	 */
	@RequestMapping(value = "/unit", method = PUT)
	public ResponseEntity<Unit> updateUnit(@Valid @RequestBody Unit unit) {
		try {
			Unit updatedUnit = new Unit();
			updatedUnit = unitService.updateUnit(unit);
			return new ResponseEntity<Unit>(updatedUnit, HttpStatus.OK);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Unit>(unit, HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Unit>(unit, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The method used to delete a unit using the unit ID.
	 * 
	 * @param teamId
	 * @return
	 */
	@RequestMapping(value = "/unit/{unitId}", method = DELETE)
	public ResponseEntity<Void> deleteUnit(@PathVariable int unitId) {

		// TODO : add validation to check whether unit is alrady started
		try {
			unitService.deleteUnit(unitId);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method to find all the current unit of the convener
	 * 
	 * @return
	 */
	@RequestMapping(value = "/units/convener/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Unit>> findAllUnitOfConvener(@PathVariable int id) {
		List<Unit> units = new ArrayList<>();
		try {
			units = unitService.findAllUnitOfConvener(id);
			return new ResponseEntity<List<Unit>>(units, HttpStatus.OK);
		} catch (NoResultException | UnitException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method to find all the current unit of the student
	 * 
	 * @return
	 */
	@RequestMapping(value = "/units/student/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Unit>> findAllUnitOfStudent(@PathVariable int id) {
		List<Unit> units = new ArrayList<>();
		try {
			units = unitService.findAllUnitOfStudent(id);

			return new ResponseEntity<List<Unit>>(units, HttpStatus.OK);
		} catch (NoResultException | UnitException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/units", method = RequestMethod.GET)
	public ResponseEntity<List<Unit>> searchUnits(@RequestParam String name, @RequestParam String code) {
		List<Unit> units = new ArrayList<>();
		try {

			units = unitService.searchUnits(name, code);
			return new ResponseEntity<List<Unit>>(units, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/units/name/{name}", method = RequestMethod.GET)
	public ResponseEntity<List<Unit>> searchUnitsByName(@PathVariable String name) {
		List<Unit> units = new ArrayList<>();
		try {
			units = unitService.findUnitByName(name);
			return new ResponseEntity<List<Unit>>(units, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/units/code/{code}", method = RequestMethod.GET)
	public ResponseEntity<List<Unit>> searchUnitsByUnitCode(@PathVariable String code) {
		List<Unit> units = new ArrayList<>();
		try {

			units = unitService.findUnitByUnitCode(code);
			return new ResponseEntity<List<Unit>>(units, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Unit>>(units, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/units/id/{id}", method = RequestMethod.GET)
	public ResponseEntity<Unit> searchUnitById(@PathVariable int id) {
		Unit unit = new Unit();
		try {
			unit = unitService.findUnitById(id);
			return new ResponseEntity<Unit>(unit, HttpStatus.OK);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Unit>(unit, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Unit>(unit, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
