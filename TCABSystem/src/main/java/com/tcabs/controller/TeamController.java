package com.tcabs.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tcabs.exception.SupervisorException;
import com.tcabs.exception.TeamException;
import com.tcabs.model.ProjectTeamAllocation;
import com.tcabs.model.Team;
import com.tcabs.service.TeamService;

@RestController
public class TeamController {

	@Autowired
	private TeamService teamService;

	/**
	 * The controller method used to get the information about a team using team
	 * id
	 * 
	 * @param teamId
	 * @return
	 */
	@RequestMapping(value = "/team/{teamId}", method = RequestMethod.GET)
	public ResponseEntity<ProjectTeamAllocation> getProjectAllocation(@PathVariable int teamId) {
		ProjectTeamAllocation projectTeamAllocation = new ProjectTeamAllocation();
		try {
			projectTeamAllocation = teamService.findTeamByTeamId(teamId);
			return new ResponseEntity<ProjectTeamAllocation>(projectTeamAllocation, HttpStatus.OK);
		} catch (TeamException ex) {
			ex.printStackTrace();
			return new ResponseEntity<ProjectTeamAllocation>(projectTeamAllocation, HttpStatus.NOT_FOUND);
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<ProjectTeamAllocation>(projectTeamAllocation, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<ProjectTeamAllocation>(projectTeamAllocation, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The method used to allocate a team to a project or update a team
	 * allocation. A list of team will be used as the input.
	 * 
	 * @param teams
	 * @return
	 */
	@RequestMapping(value = "/team", method = RequestMethod.POST)
	public ResponseEntity<List<Team>> allocateTeam(@RequestBody Team[] teams) {
		List<Team> allocatedTeam = new ArrayList<>();
		try {
			Team team = new Team();
			team = teamService.findTeamId(teams[0].getId());
			if (team == null) {
				allocatedTeam = teamService.registerTeam(teams);
			} else {
				allocatedTeam = teamService.updateTeam(teams, team);
			}
			return new ResponseEntity<List<Team>>(allocatedTeam, HttpStatus.CREATED);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(allocatedTeam, HttpStatus.NOT_ACCEPTABLE);
		} catch (TeamException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(ex.getInvalidTeams(), HttpStatus.BAD_REQUEST);
		} catch (SupervisorException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(allocatedTeam, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(allocatedTeam, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * This method used to search teams using the team name and the project id.
	 * 
	 * @param teamName
	 * @param projectId
	 * @return
	 */
	@RequestMapping(value = "/teams", method = RequestMethod.GET)
	public ResponseEntity<List<Team>> searchTeams(@RequestParam String teamName, @RequestParam int projectId) {
		List<Team> teams = new ArrayList<>();
		try {
			teams = teamService.searchTeamByTeamNameAndProjectId(teamName, projectId);
			return new ResponseEntity<List<Team>>(teams, HttpStatus.OK);
		} catch (TeamException | NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(teams, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Team>>(teams, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The method used to search for a team using team ID as the input.
	 * 
	 * @param teamIdentity
	 * @return
	 */
	@RequestMapping(value = "/team", method = RequestMethod.GET)
	public ResponseEntity<Team> searchTeam(@RequestParam int teamIdentity) {
		Team team = new Team();
		try {
			team = teamService.findTeamId(teamIdentity);
			return new ResponseEntity<Team>(team, HttpStatus.OK);
		} catch (TeamException | NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Team>(team, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Team>(team, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The method used to delete a team using the team ID.
	 * 
	 * @param teamId
	 * @return
	 */
	@RequestMapping(value = "/team/{teamId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteTeam(@PathVariable int teamId) {
		try {
			teamService.deleteTeam(teamId);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * The controller method used to get the information about a team using
	 * project id and student id
	 * 
	 * @param teamId
	 * @return
	 */
	@RequestMapping(value = "/team/{projectId}/{userId}", method = RequestMethod.GET)
	public ResponseEntity<Team> getTeamByProjectIdAndStudentId(@PathVariable int projectId,
			@PathVariable int userId) {
		Team team = new Team();
		try {
			team = teamService.findTeamByProjectIdAndStudentId(projectId, userId);
			if (team == null) {
				return new ResponseEntity<Team>(team, HttpStatus.NOT_FOUND);
			} else {
				return new ResponseEntity<Team>(team, HttpStatus.OK);
			}
		} catch (NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<Team>(team, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Team>(team, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
