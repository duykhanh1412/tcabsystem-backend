package com.tcabs.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcabs.exception.RoleException;
import com.tcabs.model.Role;
import com.tcabs.service.RoleService;

@RestController
public class RoleController {

	@Autowired
	private RoleService roleService;

	/**
	 * The controller method used to find the roles of a project
	 * 
	 * @param projectId
	 * @return
	 */
	@RequestMapping(value = "/role/{projectId}", method = RequestMethod.GET)
	public ResponseEntity<List<Role>> getRoles(@PathVariable int projectId) {
		List<Role> roles = new ArrayList<>();
		try {
			roles = roleService.findProjectRolesByProjectId(projectId);
			return new ResponseEntity<List<Role>>(roles, HttpStatus.OK);
		} catch (RoleException | NoResultException ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Role>>(roles, HttpStatus.NOT_FOUND);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Role>>(roles, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
