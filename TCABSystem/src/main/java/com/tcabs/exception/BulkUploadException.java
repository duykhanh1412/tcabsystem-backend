package com.tcabs.exception;


public class BulkUploadException extends Exception {

	private static final long serialVersionUID = 1L;

	private String errorFile;

	public BulkUploadException(String errorFile) {
		super();
		this.errorFile = errorFile;
	}

	public String getErrorFile() {
		return errorFile;
	}
}
