package com.tcabs.exception;

import com.tcabs.model.Role;

public class RoleException extends Exception {

	private static final long serialVersionUID = 1L;

	private Role role;

	public RoleException(Role role) {
		super();
		this.role = role;
	}

	public Role getRole() {
		return role;
	}
}
