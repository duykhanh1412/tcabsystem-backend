package com.tcabs.exception;

import com.tcabs.model.Student;

public class StudentException extends Exception {

	private static final long serialVersionUID = 1L;

	private Student student;

	public StudentException(Student student) {
		super();
		this.student = student;
	}

	public Student getStudent() {
		return student;
	}
}
