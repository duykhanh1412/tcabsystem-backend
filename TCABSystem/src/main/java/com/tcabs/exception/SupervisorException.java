package com.tcabs.exception;

import com.tcabs.model.Supervisor;

public class SupervisorException extends Exception {

	private static final long serialVersionUID = 1L;

	private Supervisor supervisor;

	public SupervisorException(Supervisor supervisor) {
		super();
		this.supervisor = supervisor;
	}

	public Supervisor getSupervisor() {
		return supervisor;
	}
}
