package com.tcabs.exception;

import com.tcabs.model.Convener;

public class ConvenerException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private Convener convener;

	public ConvenerException(Convener convener) {
		super();
		this.convener = convener;
	}

	public Convener getSupervisor() {
		return convener;
	}
}
