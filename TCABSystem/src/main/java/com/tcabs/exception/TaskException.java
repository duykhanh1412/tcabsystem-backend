package com.tcabs.exception;

import com.tcabs.model.Task;

public class TaskException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private Task task;

	public TaskException(Task task) {
		super();
		this.task = task;
	}
	
	public Task getTeam() {
		return task;
	}
}
