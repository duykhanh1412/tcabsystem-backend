package com.tcabs.exception;

import com.tcabs.model.User;

public class UserEmailException extends UserException{

	private static final long serialVersionUID = 1L;

	public UserEmailException(User user){
		this.user = user;
	}
}