package com.tcabs.exception;

import com.tcabs.model.Project;

public class ProjectException extends Exception {

	private static final long serialVersionUID = 1L;

	private Project project;

	public ProjectException(Project project) {
		super();
		this.project = project;
	}

	public Project getUnit() {
		return project;
	}
}
