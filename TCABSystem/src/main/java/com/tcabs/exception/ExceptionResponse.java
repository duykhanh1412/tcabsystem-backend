package com.tcabs.exception;

public class ExceptionResponse {
	private int code;

	private String message;

	private TcabException error;

	public ExceptionResponse(int code, String message, TcabException error) {
        this.code = code;
        this.message = message;
        this.error = error;
    }

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TcabException getError() {
		return error;
	}

	public void setError(TcabException error) {
		this.error = error;
	}
}
