package com.tcabs.exception;

import com.tcabs.model.Unit;

public class UnitException extends Exception {

	private static final long serialVersionUID = 1L;

	private Unit unit;

	public UnitException(Unit unit) {
		super();
		this.unit = unit;
	}

	public Unit getUnit() {
		return unit;
	}
}
