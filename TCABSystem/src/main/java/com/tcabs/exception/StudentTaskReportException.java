package com.tcabs.exception;

import java.util.List;

import com.tcabs.model.StudentTaskReport;

public class StudentTaskReportException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private List<StudentTaskReport> studentTaskReports;

	public StudentTaskReportException(List<StudentTaskReport> studentTaskReports) {
		super();
		this.studentTaskReports = studentTaskReports;
	}

	public List<StudentTaskReport> getStudentTaskReports() {
		return studentTaskReports;
	}
}
