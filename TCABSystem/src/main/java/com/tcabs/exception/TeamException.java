package com.tcabs.exception;

import java.util.List;

import com.tcabs.model.Team;

public class TeamException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private Team team;
	
	private List<Team> invalidTeams;

	public TeamException(Team team) {
		super();
		this.team = team;
	}

	public TeamException(List<Team> invalidTeams) {
		super();
		this.invalidTeams = invalidTeams;
	}

	public List<Team> getInvalidTeams() {
		return invalidTeams;
	}
	
	public Team getTeam() {
		return team;
	}
}
