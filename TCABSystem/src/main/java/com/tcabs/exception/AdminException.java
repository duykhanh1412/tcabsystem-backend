package com.tcabs.exception;

import com.tcabs.model.User;

public class AdminException extends Exception {

	private static final long serialVersionUID = 1L;

	protected User user;

	public AdminException(User user) {
		super();
		this.user = user;
	}

	public String getEmail() {
		return user.getEmail();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
