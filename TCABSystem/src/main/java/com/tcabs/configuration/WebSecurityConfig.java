package com.tcabs.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import com.tcabs.configuration.security.AuthorizationFailureHandler;
import com.tcabs.configuration.security.AuthenticationFailureHandler;
import com.tcabs.configuration.security.AuthenticationSuccessHandler;
import com.tcabs.configuration.security.AuthenticationEntryPointHandler;;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan({"com.tcabs.configuration.security"})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	public WebSecurityConfig() {
		super();
	}

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private AuthenticationEntryPointHandler authenticationEntryPointHandler;

	@Autowired
	private AuthorizationFailureHandler authorizationFailureHandler;

	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;

	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}

	@Override
	public void configure(WebSecurity webSecurity) throws Exception {

	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.
			headers().disable().csrf().disable()
			.authorizeRequests()
				.antMatchers("/user").permitAll()
				.antMatchers("/unit").hasAnyAuthority("ADMIN")
				.antMatchers("/unit/{unitId}").hasAnyAuthority("ADMIN")
				.antMatchers("/units").hasAnyAuthority("ADMIN")
				.antMatchers("/units/student/{id}").hasAnyAuthority("ADMIN","SUPERVISOR","CONVENER","STUDENT")
				.antMatchers("/units/convener/{id}").hasAnyAuthority("ADMIN","SUPERVISOR","CONVENER","STUDENT")
				.antMatchers("/employee/{employeeId}").hasAnyAuthority("ADMIN")
				.antMatchers("/employee").hasAnyAuthority("ADMIN")
				.antMatchers("/student").hasAnyAuthority("ADMIN")
				.antMatchers("/student/{studentId}").hasAnyAuthority("ADMIN")
				.antMatchers("/student").hasAnyAuthority("ADMIN")
				.antMatchers("/student/enroll/{errorFile}").hasAnyAuthority("ADMIN")
				.antMatchers("/student/enroll").hasAnyAuthority("ADMIN")
				.antMatchers("/students").hasAnyAuthority("CONVENER", "SUPERVISOR")
				.antMatchers("/students/{teamId}/{projectId}").hasAnyAuthority("CONVENER", "SUPERVISOR")
				.antMatchers("/supervisor").hasAnyAuthority("CONVENER")
				.antMatchers("/project").hasAnyAuthority("CONVENER")
				.antMatchers("/project/{projectId}").hasAnyAuthority("CONVENER")
				.antMatchers("/project/description/{projectId}").hasAnyAuthority("CONVENER")
				.antMatchers("/project/allocation").hasAnyAuthority("CONVENER")
				.antMatchers("/project/allocation/{teamId}").hasAnyAuthority("CONVENER")
				.antMatchers("/project/allocation/{projectId}").hasAnyAuthority("CONVENER")
				.antMatchers("/projects/{unitId}").hasAnyAuthority("CONVENER", "STUDENT")
				.antMatchers("/team").hasAnyAuthority("CONVENER")
				.antMatchers("/team/{teamId}").hasAnyAuthority("CONVENER")
				.antMatchers("/team/{projectId}/{userId}").hasAnyAuthority("CONVENER", "SUPERVISOR", "STUDENT")
				.antMatchers("/teams").hasAnyAuthority("CONVENER", "SUPERVISOR")
				.antMatchers("/report/studentTask/{teamId}/{userId}").hasAnyAuthority("CONVENER", "SUPERVISOR")
				.antMatchers("/report/weeklyStudentTask/{teamId}").hasAnyAuthority("CONVENER", "SUPERVISOR")
				.antMatchers("/report/teamTask/{teamId}").hasAnyAuthority("CONVENER", "SUPERVISOR")
				.antMatchers("/role/{projectId}").hasAnyAuthority("STUDENT")
				.antMatchers("/task").hasAnyAuthority("STUDENT")
				.antMatchers("/security/account").permitAll()
				.anyRequest().authenticated()
				.and()
			.exceptionHandling()
				.accessDeniedHandler(authorizationFailureHandler)
				.authenticationEntryPoint(authenticationEntryPointHandler)
				.and()
			.formLogin()
				.loginProcessingUrl("/authenticate")
				.usernameParameter("username")
				.passwordParameter("password")
				.successHandler(authenticationSuccessHandler)
				.failureHandler(authenticationFailureHandler)
				.permitAll()
				.and()
			.logout()
				.logoutUrl("/logout")
				.logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler())
				.deleteCookies("JSESSIONID")
				.permitAll()
				.and().
			rememberMe()
				.tokenValiditySeconds(2419200)
				.key("UserKey");
	}
}
