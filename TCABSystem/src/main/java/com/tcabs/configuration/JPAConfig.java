package com.tcabs.configuration;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@Component
@EnableJpaRepositories(basePackages = { "com.tcabs.model", "com.tcabs.repository" }, entityManagerFactoryRef = "emf")
@EnableTransactionManagement
public class JPAConfig {

	@Value("${spring.datasource.driverclassname}")
	private String dirverClassName;

	@Value("${spring.datasource.url}")
	private String dbConnectionUrl;

	@Value("${spring.datasource.username}")
	private String userName;

	@Value("${spring.datasource.password}")
	private String password;

	@Value("${spring.datasource.initialise-size}")
	private int initialiseSize;

	@Value("${spring.datasource.max-size}")
	private int maxTotal;

	@Value("${spring.jpa.show-sql}")
	private boolean showSql;

	@Value("${spring.jpa.hibernate.generate-ddl}")
	private boolean generateDdl;

	@Value("${spring.jpa.properties.hibernate.dialect}")
	private String databasePlatform;

	@Bean
	public DataSource dataSource() {
		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName(dirverClassName);
		ds.setUrl(dbConnectionUrl);
		ds.setUsername(userName);
		ds.setPassword(password);
		ds.setInitialSize(initialiseSize);
		ds.setMaxTotal(maxTotal);
		return ds;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean emf(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
		LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
		emf.setDataSource(dataSource);
		emf.setJpaVendorAdapter(jpaVendorAdapter);
		emf.setPackagesToScan("com.tcabs.model");
		return emf;
	}

	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setDatabase(Database.MYSQL);
		adapter.setShowSql(showSql);
		adapter.setGenerateDdl(generateDdl);
		adapter.setDatabasePlatform(databasePlatform);
		return adapter;
	}

	@Bean
	JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}
}
