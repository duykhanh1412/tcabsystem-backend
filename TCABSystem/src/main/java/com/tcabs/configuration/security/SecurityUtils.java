package com.tcabs.configuration.security;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tcabs.exception.ExceptionResponse;
import com.tcabs.exception.TcabException;

public class SecurityUtils {

	private static final ObjectMapper mapper = new ObjectMapper();

	public SecurityUtils() {
		super();
	}
	
	@SuppressWarnings("null")
	public static String getCurrentLoginUser(){
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication authentication = securityContext.getAuthentication();
		UserDetails loginUser = null;
		String userName = null;
		
		if(authentication != null){
			if(authentication.getPrincipal() instanceof UserDetails){
				loginUser = (UserDetails) authentication.getPrincipal();
				userName = loginUser.getUsername();
			}
		} else if(authentication.getPrincipal() instanceof String){
			userName = (String) authentication.getPrincipal();
		}
		return userName;
	}
	
	public static void sendError(HttpServletResponse response, Exception exception, int status, String message) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(status);
        PrintWriter writer = response.getWriter();
        TcabException tcabException = new TcabException("authError", exception.getMessage());
        writer.write(mapper.writeValueAsString(new ExceptionResponse(status, message, tcabException)));
        writer.flush();
        writer.close();
    }


    public static void sendResponse(HttpServletResponse response, int status, Object object) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        writer.write(mapper.writeValueAsString(object));
        response.setStatus(status);
        writer.flush();
        writer.close();
    }
}
